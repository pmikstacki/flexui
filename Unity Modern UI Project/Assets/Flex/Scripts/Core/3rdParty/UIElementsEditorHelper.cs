﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

namespace Flex.Scripts.Core._3rdParty
{
    public static class UIElementsEditorHelper
    {
        public static IEnumerable<string> SplitCamelCase(this string source)
        {
            const string pattern = @"[A-Z][a-z]*|[a-z]+|\d+";
            var matches = Regex.Matches(source, pattern);
            foreach (Match match in matches)
            {
                yield return match.Value;
            }
        }
        
        public static void FillDefaultInspector(VisualElement container, SerializedObject serializedObject, bool hideScript)
        {
            SerializedProperty property = serializedObject.GetIterator();
            if (property.NextVisible(true)) // Expand first child.
            {
                do
                {
                    if (property.propertyPath == "m_Script" && hideScript)
                    {
                        continue;
                    }
                    var field = new PropertyField(property);
                    field.name = "PropertyField:" + property.propertyPath;
     
     
                    if (property.propertyPath == "m_Script" && serializedObject.targetObject != null)
                    {
                        field.SetEnabled(false);
                    }
     
                    container.Add(field);
                }
                while (property.NextVisible(false));
            }
        }
    }

}