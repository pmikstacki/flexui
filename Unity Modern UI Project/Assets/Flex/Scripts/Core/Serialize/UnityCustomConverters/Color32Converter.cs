﻿using System;
using System.IO;
using UnityEngine;
using YamlDotNet.Core;
using YamlDotNet.Core.Events;
using YamlDotNet.Serialization;

namespace Flex.Scripts.Core.Serialize.UnityCustomConverters
{
    internal sealed class Color32Converter : IYamlTypeConverter
    {
        public bool Accepts(Type type)
        {
            if (type == typeof(Color32))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Color32Converter()
        {
        }


        public object ReadYaml(IParser parser, Type type)
        {
            if (parser.Current.GetType() != typeof(MappingStart)) // You could also use parser.Accept<MappingStart>()
            {
                throw new InvalidDataException("Invalid YAML content.");
            }        
            parser.MoveNext(); // move on from the map start
            var result = new Color32();
            string[] values = new string[8];
            int i = 0;
            do
            {
                var r = parser.Current as Scalar;
                if (r == null)
                {
                    
                    throw new InvalidDataException("Failed to retrieve scalar value for color.");
                }
                else
                {
                    values[i] = r.Value;
                    i++;
                }
                parser.MoveNext();
            }
            while(parser.Current.GetType() != typeof(MappingEnd));

            result.r = byte.Parse(values[1]);
            result.g = byte.Parse(values[3]);
            result.b = byte.Parse(values[5]);
            result.a = byte.Parse(values[7]);
            
            parser.MoveNext();
            return result;
        }

        public void WriteYaml(IEmitter emitter, object value, Type type)
        {
            var color = (Color32) value;
            
            emitter.Emit(new MappingStart(null, null, false, MappingStyle.Block));
            emitter.Emit(new Scalar("r"));
            emitter.Emit(new Scalar(color.r.ToString()));

            emitter.Emit(new Scalar("g"));
            emitter.Emit(new Scalar(color.g.ToString()));

            emitter.Emit(new Scalar("b"));
            emitter.Emit(new Scalar(color.b.ToString()));
            
            emitter.Emit(new Scalar("a"));
            emitter.Emit(new Scalar(color.a.ToString()));

            
            emitter.Emit(new MappingEnd());
        }
    }
}