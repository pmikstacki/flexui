﻿using System;
using System.IO;
using UnityEngine;
using YamlDotNet.Core;
using YamlDotNet.Core.Events;
using YamlDotNet.Serialization;

namespace Flex.Scripts.Core.Serialize.UnityCustomConverters
{
    internal sealed class ColorConverter : IYamlTypeConverter
    {
        public bool Accepts(Type type)
        {
            if (type == typeof(Color))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public ColorConverter()
        {
        }

        
        public object ReadYaml(IParser parser, Type type)
        {
            if (parser.Current.GetType() != typeof(MappingStart)) // You could also use parser.Accept<MappingStart>()
            {
                throw new InvalidDataException("Invalid YAML content.");
            }        
            parser.MoveNext(); // move on from the map start
            var result = new Color();
            string[] values = new string[8];
            int i = 0;
            do
            {
                var r = parser.Current as Scalar;
                if (r == null)
                {
                    
                    throw new InvalidDataException("Failed to retrieve scalar value for color.");
                }
                else
                {
                    values[i] = r.Value;
                    i++;
                }
                parser.MoveNext();
            }
            while(parser.Current.GetType() != typeof(MappingEnd));

            result.r = float.Parse(values[1]);
            result.g = float.Parse(values[3]);
            result.b = float.Parse(values[5]);
            result.a = float.Parse(values[7]);
            
            parser.MoveNext();
            return result;
        }

        public void WriteYaml(IEmitter emitter, object value, Type type)
        {
            var color = (Color) value;
            
            emitter.Emit(new MappingStart(null, null, false, MappingStyle.Block));
            emitter.Emit(new Scalar("r"));
            emitter.Emit(new Scalar(color.r.ToString()));

            emitter.Emit(new Scalar("g"));
            emitter.Emit(new Scalar(color.g.ToString()));

            emitter.Emit(new Scalar("b"));
            emitter.Emit(new Scalar(color.b.ToString()));
            
            emitter.Emit(new Scalar("a"));
            emitter.Emit(new Scalar(color.a.ToString()));

            
            emitter.Emit(new MappingEnd());
        }
    }
}