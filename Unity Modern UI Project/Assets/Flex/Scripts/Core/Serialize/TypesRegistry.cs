﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Flex.Scripts.Core.Serialize.Storage;
using UnityEditor;
using UnityEngine;

namespace Flex.Scripts.Core.Serialize
{
    public static class TypesRegistry
    {
        private static List<Type> _types = new List<Type>();

        public static List<Type> Types => _types;

        public static void RegisterBaseType(Type t)
        {
            if(!_types.Contains(t))
                _types.Add(t);

            Save();
        }
        public static void UnregisterBaseType(Type t)
        {
            if(_types.Contains(t))
                _types.Remove(t);
            Save();
        }
        public static bool HasBaseType(Type t)
        {
            return _types.Contains(t);
        }
        public static void RegisterAsBaseType(this object objectType)
        {
            var t = objectType.GetType();
            if(!_types.Contains(t))
                _types.Add(t);

            Save();
        }

        private static void Save()
        {
            var types = JsonUtility.ToJson(_types);
            PlayerPrefs.SetString("Flex_TypeRegistry", types);
        }
        private static void Load()
        {
            if (PlayerPrefs.HasKey("Flex_TypeRegistry"))
            {
                _types = JsonUtility.FromJson<List<Type>>(PlayerPrefs.GetString("Flex_TypeRegistry"));
            }
            else
            {
                _types = new List<Type>();
            }
        }
        static TypesRegistry()
        {
            Load();
        }
    }
}