using System;
using System.Collections.Generic;
using UnityEngine;

namespace Flex.Scripts.Core.Serialize.AbstractHandlers
{
    public class ExpectationTypeResolver : ITypeDiscriminator
    {
        private readonly Dictionary<string, Type> typeLookup;

        public ExpectationTypeResolver(Type baseType)
        {
            typeLookup = new Dictionary<string, Type>();
            foreach (var type in ReflectiveEnumerator.GetEnumerableOfType(baseType))
            {
               //Debug.Log("ExpectationResolver here. type is: "+type.FullName);
                typeLookup.Add(type.FullName, type);
            }

            BaseType = baseType;
        }

        public Type BaseType { get; private set;  }

        public bool TryResolve(ParsingEventBuffer buffer, out Type suggestedType)
        {
            //Generalnie tutaj trzeba ogarnąć, w jaki sposób identyfikować clasy pochodne tak aby były castowane do odpowiednich typów
            //Na zasadzie getuje type - porównuje nazwy pól z typem bazowym

            //Debug.Log($"Trying to resolve {buffer.Current.End}");
            if (buffer.TryFindingFittingType(BaseType, out suggestedType))
            {
                return true;
            }

            suggestedType = null;
            return false;
        }
    }
}