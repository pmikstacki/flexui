using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;
using YamlDotNet.Core;
using YamlDotNet.Core.Events;

namespace Flex.Scripts.Core.Serialize.AbstractHandlers
{
    public static class IParserExtensions
    {
        
        public static bool TryFindingFittingType(this ParsingEventBuffer parser, Type baseType, out Type suggestedType)
        {
            parser.Consume<MappingStart>();
            List<string> names = new List<string>();
            List<string> values = new List<string>();
            int i = 1;
            do
            {
                Debug.Log(parser.Current.ToString()); 
                // so we only want to check keys in this mapping, don't descend
                switch (parser.Current)
                {
                    case Scalar scalar:
                        // we've found a scalar, check if it's value matches one
                        // of our  predicates
                        //Debug.Log("["+i+"]Scalar value: "+scalar.Value);
                        if (i % 2 != 0)
                        {
                            names.Add(scalar.Value);
                        }
                        else
                        {
                            values.Add(scalar.Value);
                        }

                        i++;
                        parser.MoveNext();
                        break;
                    case MappingStart mappingStart:
                        parser.SkipThisAndNestedEvents();
                        break;
                    case SequenceStart sequenceStart:
                        parser.SkipThisAndNestedEvents();
                        break;
                    default:
                        // do nothing, skip to next node
                        parser.MoveNext();
                        break;
                }
            } while (!(parser.Current is null));

            //Debug.Log($"Searching for compatible type. Names: {string.Join(",",names)}\nvalues{string.Join(",",values)}");
            
            var fitting = FindFittingType(names, baseType);
            if (fitting != null)
            {
                suggestedType = fitting;
                return true;
            }
            else
            {
                suggestedType = null;
                return false;
            }
        }

        
        public static bool TryFindMappingEntry(this ParsingEventBuffer parser, Func<Scalar, bool> selector, out Scalar key, out ParsingEvent value)
        {
            parser.Consume<MappingStart>();
            do
            {
                Debug.Log(parser.Current.ToString()); 
                // so we only want to check keys in this mapping, don't descend
                switch (parser.Current)
                {
                    case Scalar scalar:
                        // we've found a scalar, check if it's value matches one
                        // of our  predicates
                        var keyMatched = selector(scalar);

                        // move head so we can read or skip value
                        parser.MoveNext();

                        // read the value of the mapping key
                        if (keyMatched)
                        {
                            // success
                            value = parser.Current;
                            key = scalar;
                            return true;
                        }
                        // skip the value
                        parser.SkipThisAndNestedEvents();
                        break;
                    case MappingStart mappingStart:
                        parser.SkipThisAndNestedEvents();
                        break;
                    case SequenceStart sequenceStart:
                        parser.SkipThisAndNestedEvents();
                        break;
                    default:
                        // do nothing, skip to next node
                        parser.MoveNext();
                        break;
                }
            } while (!(parser.Current is null));

            key = null;
            value = null;
            return false;
        }

        public static Type FindFittingType(List<string> memberNames, Type baseType)
        {
            Dictionary<Type, List<string>> typeDict = new Dictionary<Type, List<string>>();
            foreach (var type in Assembly.GetAssembly(baseType)
                .GetTypes()
                .Where(myType => myType.IsClass && !myType.IsAbstract && myType.IsSubclassOf(baseType))
                .ToList())
            {
                var list = new List<string>();
                list.AddRange(type.GetFields().Select(x => x.Name));
                list.AddRange(type.GetProperties().Select(x => x.Name));
                typeDict.Add(type, list);
            }

            foreach (var t in typeDict)
            {
                if (t.Value.OrderBy(fElement => fElement).SequenceEqual(memberNames.OrderBy(sElement => sElement)))
                {
                    return t.Key;
                }
            }

            return null;
        }

    }
}