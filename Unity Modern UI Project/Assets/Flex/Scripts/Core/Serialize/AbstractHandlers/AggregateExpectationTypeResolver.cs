using System;
using System.Collections.Generic;
using UnityEngine;
using YamlDotNet.Core.Events;

namespace Flex.Scripts.Core.Serialize.AbstractHandlers
{
    public class AggregateExpectationTypeResolver : ITypeDiscriminator
    {
        public string TargetKey;
        private readonly string targetKey;
        private readonly Dictionary<string, Type> typeLookup;
 
        public AggregateExpectationTypeResolver(Type baseType)
        {
            BaseType = baseType;
            TargetKey = nameof(baseType);
            typeLookup = new Dictionary<string, Type>();
            foreach (var type in ReflectiveEnumerator.GetEnumerableOfType(baseType))
            { 
                typeLookup.Add(type.FullName, type);  
            }
            
        }

        public Type BaseType { get; private set; }

        public bool TryResolve(ParsingEventBuffer buffer, out Type suggestedType)
        {
            if (buffer.TryFindMappingEntry(
                scalar => targetKey == scalar.Value,
                out Scalar key,
                out ParsingEvent value))
            {
                // read the value of the kind key
                if (value is Scalar valueScalar)
                {
                    
                    suggestedType = CheckName(valueScalar.Value);

                    return true;
                }
                else
                {
                    FailEmpty();
                }
            }

            // we could not find our key, thus we could not determine correct child type
            suggestedType = null;
            return false;
        }


        private void FailEmpty()
        {
            throw new Exception($"Could not determin expectation type, {targetKey} has an empty value");
        }

        private Type CheckName(string value)
        {
            Debug.Log("Checking name Type for: "+value);

            if (typeLookup.TryGetValue(value, out var childType))
            {
                return childType;
            }



            var known = string.Join(",", typeLookup.Keys);
            throw new Exception($"Could not match `{targetKey}: {value} to a known expectation. Expecting one of: {known}");
        }
    }
}