using System;

namespace Flex.Scripts.Core.Serialize.AbstractHandlers
{
    public interface ITypeDiscriminator
    {
        Type BaseType { get; }

        bool TryResolve(ParsingEventBuffer buffer, out Type suggestedType);
    }
}