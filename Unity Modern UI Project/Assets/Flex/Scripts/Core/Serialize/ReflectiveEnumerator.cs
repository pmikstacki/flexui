﻿/* GOD BLESS STACKOVERFLOW
 * https://stackoverflow.com/questions/5411694/get-all-inherited-classes-of-an-abstract-class
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Flex.Scripts.Core.Serialize
{
    public static class ReflectiveEnumerator
    {
        static ReflectiveEnumerator()
        {
        }
        
        public static IEnumerable<Type> GetEnumerableOfTypeInterface<T>() where T : class
        {
            var objects = AppDomain.CurrentDomain.GetAssemblies().SelectMany( a => a
                    .GetTypes()
                    .Where(myType => !myType.IsInterface && !myType.IsAbstract && myType.IsAssignableFrom(typeof(T))))
                .ToList();
            
            return objects;
        } 
        
        public static IEnumerable<Type> GetEnumerableOfType<T>() where T : class
        {
            var objects = AppDomain.CurrentDomain.GetAssemblies().SelectMany( a => a
                    .GetTypes()
                    .Where(myType => myType.IsClass && !myType.IsAbstract && myType.IsSubclassOf(typeof(T))))
                .ToList();
            
            return objects;
        } 
        public static IEnumerable<Type> GetEnumerableOfType(Type type)
        {
            var objects = AppDomain.CurrentDomain.GetAssemblies().SelectMany( a => a
                .GetTypes()
                .Where(myType => myType.IsClass && !myType.IsAbstract && myType.IsSubclassOf(type)))
                .ToList();
            
            return objects;
        }
        public static IEnumerable<T> GetEnumerableOfType<T>(params object[] constructorArgs) where T : class
        {
            List<T> objects = new List<T>();
            foreach (Type type in
                Assembly.GetAssembly(typeof(T)).GetTypes()
                    .Where(myType => myType.IsClass && !myType.IsAbstract && myType.IsSubclassOf(typeof(T))))
            {
                objects.Add((T) Activator.CreateInstance(type, constructorArgs));
            }

            return objects;
        }

    }
}