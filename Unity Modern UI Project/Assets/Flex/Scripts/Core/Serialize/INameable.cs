﻿namespace Flex.Scripts.Core.Serialize
{
    public interface INameable
    {
        public string Name { get; set; }
    }
}