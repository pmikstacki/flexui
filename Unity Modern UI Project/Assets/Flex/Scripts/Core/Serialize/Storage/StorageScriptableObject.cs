﻿using UnityEngine;

namespace Flex.Scripts.Core.Serialize.Storage
{
    public abstract class StorageScriptableObject<T> : ScriptableObject where T : class
    {
    public string Key { get; set; }

    public DataStoreBase<T> Storebase { get; set; }

    }
}