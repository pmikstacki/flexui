﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Flex.Scripts.Core.Serialize.AbstractHandlers;
using Flex.Scripts.Core.Serialize.UnityCustomConverters;
using UnityEditor;
using UnityEngine;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NodeDeserializers;

namespace Flex.Scripts.Core.Serialize.Storage
{
    /// <summary>
    /// Stores data serialized in editor. Use for Editor Windows / Editor Modules
    /// </summary>
    public class ImportableEditorDataStore<TImportable> : DataStoreBase<TImportable>  where TImportable : class, INameable
    {
        
        public TImportable this[string name]
        {
            get
            {
                return _databaseToStore.First(x => x.Name ==name);
            }

            set
            {
                if (_databaseToStore.All(x => x.Name != name))
                {
                    Add(value);
                }
                else
                {
                    _databaseToStore[_databaseToStore.IndexOf(_databaseToStore.First(x => x.Name == name))] = value;
                }
            }
        }
        
        public override void Save()
        {
            var serializer = new SerializerBuilder().WithTypeConverter(new ColorConverter()).Build();
            File.WriteAllText(Path.Combine(Application.dataPath, $"Flex\\{Key}_Database"), serializer.Serialize(_databaseToStore));
        }

        public override void Load()
        {
            try  
            {
                var text = File.ReadAllText(Path.Combine(Application.dataPath, $"Flex\\{Key}_Database"));
                if (!string.IsNullOrEmpty(text))
                {
                    List<ITypeDiscriminator> discriminators = new List<ITypeDiscriminator>();
                    
                    foreach (Type t in TypesRegistry.Types)
                    {
                        Debug.Log(t.ToString());
                        discriminators.Add(new ExpectationTypeResolver(t));
                    }


                    var deserializerBuilder = new DeserializerBuilder().WithNodeDeserializer(
                        inner => new AbstractNodeNodeTypeResolver(inner, discriminators.ToArray()),
                        s => s.InsteadOf<ObjectNodeDeserializer>());
                    foreach (Type t in ReflectiveEnumerator.GetEnumerableOfTypeInterface<IYamlTypeConverter>())
                    {
                        if (Activator.CreateInstance(t) is IYamlTypeConverter converter)
                        {
                            deserializerBuilder.WithTypeConverter(converter);
                        }
                    }

                    //Tutaj zamiast build od razu przeloopować przez typy abstract wybrane przez użytkownika
                    
                    _databaseToStore = deserializerBuilder.Build().Deserialize<List<TImportable>>(text);
                }
                else
                {
                    _databaseToStore = new List<TImportable>();
                }
            }
            catch (FileNotFoundException fileNotFoundException)
            {
                Debug.Log("Database not created, creating a new one");
                var db = new List<TImportable>();
                var serializer = new Serializer();
                File.WriteAllText(Path.Combine(Application.dataPath, $"Flex\\{Key}_Database"),serializer.Serialize(db) );
                _databaseToStore = db;
            }
        }

        public void Import(string filePathToImport)
        {
            try  
            {
                var text = File.ReadAllText(filePathToImport);
                if (!string.IsNullOrEmpty(text))
                {
                    List<ITypeDiscriminator> discriminators = new List<ITypeDiscriminator>();
                    
                    foreach (Type t in TypesRegistry.Types)
                    {
                        discriminators.Add(new ExpectationTypeResolver(t));
                    }
                    
                    
                    var deserializerBuilder = new DeserializerBuilder().WithNodeDeserializer(
                        inner => new AbstractNodeNodeTypeResolver(inner, discriminators.ToArray()),
                        s => s.InsteadOf<ObjectNodeDeserializer>()).WithTypeConverter(new ColorConverter());
                    
                    foreach (Type t in ReflectiveEnumerator.GetEnumerableOfTypeInterface<IYamlTypeConverter>())
                    {
                        if (Activator.CreateInstance(t) is IYamlTypeConverter converter)
                        {
                            deserializerBuilder.WithTypeConverter(converter);
                        }
                    }

                    var deserializedData = deserializerBuilder.Build().Deserialize<List<TImportable>>(text);
                    deserializedData.ForEach(x =>
                    {
                        if (this.All(a => a.Name != x.Name))
                        {
                            Add(x);
                        }
                        else
                        {
                            #if UNITY_EDITOR
                            if (EditorUtility.DisplayDialog("Import Warning",
                                $"The item {x.Name} you're trying to import exists in the Store. Do you want to replace it?",
                                "Yes", "No"))
                            {
                                this[x.Name] = x;
                            }
                            #endif
                        }
                    });
                }
                else
                {
                    _databaseToStore = new List<TImportable>();
                }
            }
            catch (FileNotFoundException fileNotFoundException)
            {
                Debug.Log("Database not created, creating a new one");
                var db = new List<TImportable>();
                var serializer = new Serializer();
                File.WriteAllText(Path.Combine(Application.dataPath, $"Flex\\{Key}_Database"),serializer.Serialize(db) );
                _databaseToStore = db;
            }
        }
        
        public ImportableEditorDataStore(string key) : base(key)
        {
        }
    }
}