﻿using System;
using System.Collections.Generic;
using System.IO;
using Flex.Scripts.Core.Serialize.AbstractHandlers;
using Flex.Scripts.Core.Serialize.UnityCustomConverters;
using UnityEngine;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NodeDeserializers;

namespace Flex.Scripts.Core.Serialize.Storage
{
    /// <summary>
    /// Stores data serialized in editor. Use for Editor Windows / Editor Modules
    /// </summary>
    public class EditorDataStore<T> : DataStoreBase<T> where T : class
    {
        public override void Save()
        {
            var serializer = new SerializerBuilder().WithTypeConverter(new ColorConverter()).Build();
            File.WriteAllText(Path.Combine(Application.dataPath, $"Flex\\{Key}_Database"), serializer.Serialize(_databaseToStore));
        }

        public override void Load()
        {
            try  
            {
                var text = File.ReadAllText(Path.Combine(Application.dataPath, $"Flex\\{Key}_Database"));
                if (!string.IsNullOrEmpty(text))
                {
                    List<ITypeDiscriminator> discriminators = new List<ITypeDiscriminator>();
                    
                    foreach (Type t in TypesRegistry.Types)
                    {

                        discriminators.Add(new AggregateExpectationTypeResolver(t));
                        discriminators.Add(new ExpectationTypeResolver(t));
                    }
                    
                    var deserializerBuilder = new DeserializerBuilder().WithNodeDeserializer(
                        inner => new AbstractNodeNodeTypeResolver(inner, discriminators.ToArray()),
                        s => s.InsteadOf<ObjectNodeDeserializer>());

                    foreach (Type t in ReflectiveEnumerator.GetEnumerableOfTypeInterface<IYamlTypeConverter>())
                    {
                        if (Activator.CreateInstance(t) is IYamlTypeConverter converter)
                        {
                            deserializerBuilder.WithTypeConverter(converter);
                        }
                    }
                    _databaseToStore = deserializerBuilder.Build().Deserialize<List<T>>(text);
                }
                else
                {
                    _databaseToStore = new List<T>();
                }
            }
            catch (FileNotFoundException fileNotFoundException)
            {
                Debug.Log("Database not created, creating a new one");
                var db = new List<T>();
                var serializer = new Serializer();
                File.WriteAllText(Path.Combine(Application.dataPath, $"Flex\\{Key}_Database"),serializer.Serialize(db) );
                _databaseToStore = db;
            }
        }

       
        
        public EditorDataStore(string key) : base(key)
        {
        }
    }
}