using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Flex.Scripts.Core.Serialize.Storage
{
    /// <summary>
    /// This class provides the way of storing data in serialized (yaml) form
    /// </summary>
    [Serializable]
    public abstract class DataStoreBase<T> : IQueryable<T> where T : class 
    {
        public string Key { get; }
        protected List<T> _databaseToStore = new List<T>();
        public DataStoreBase(string key)
        {
            Key = key;
            Load();
        }
  
        public T this[int index]
        {
            get
            {
                return _databaseToStore[index];
            }

            set
            {
                _databaseToStore[index] = value;
            }
        }
        
        public void Add(T item)
        {
            _databaseToStore.Add(item);
            Save();
        }

        public int IndexOf(T item)
        {
            return _databaseToStore.IndexOf(item);
        }
        
        public void Remove(T item)
        {
            _databaseToStore.Remove(item);
            Save();
        }

        public void RemoveRange(int index, int count)
        {
            _databaseToStore.RemoveRange(index, count);
            Save();
        }

        public void RemoveAt(int index)
        {
            _databaseToStore.RemoveAt(index);
            Save();
        }

        public void RemoveAll(Predicate<T> predicate)
        {
            _databaseToStore.RemoveAll(predicate);
            Save();
        }

        public List<T> GetAll() => _databaseToStore;
        
        /// <summary>
        /// Saves the data stored
        /// </summary>
        public abstract void Save();
        /// <summary>
        /// Loads Data
        /// </summary>
        /// <returns></returns>
        public abstract void Load();

        public IEnumerator<T> GetEnumerator()
        {
            return _databaseToStore.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _databaseToStore.GetEnumerator();
        }

        public Type ElementType => typeof(T);
        public Expression Expression => _databaseToStore.AsQueryable().Expression;
        public IQueryProvider Provider => _databaseToStore.AsQueryable().Provider;
    }
}
