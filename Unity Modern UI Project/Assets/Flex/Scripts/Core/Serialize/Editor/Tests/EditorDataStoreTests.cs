using System.Collections;
using System.Collections.Generic;
using Flex.Scripts.Core.Serialize;
using Flex.Scripts.Core.Serialize.Storage;
using UnityEngine;
using NUnit.Framework;
using UnityEditor.TestRunner;

public class EditorDataStoreTests
{
    
    [Test]
    public void SavingLoadingSerializedData()
    {
        TypesRegistry.RegisterBaseType(typeof(TestClass));
        EditorDataStore<TestClass> _dataStore = new EditorDataStore<TestClass>("TestDataStore");
        var list = _dataStore.GetAll();
        _dataStore = null;
        _dataStore = new EditorDataStore<TestClass>("TestDataStore");
        Assert.AreEqual(list, _dataStore);
    }

    [Test]
    public void AbstractTypesRegistryTest()
    {
        TypesRegistry.RegisterBaseType(typeof(BaseTestType));
        EditorDataStore<BaseTestType> dataStore = new EditorDataStore<BaseTestType>("TestAbstractDataStore");
        dataStore.Add(new DerrivedTestType());
        var afterList = dataStore.GetAll();
        dataStore = null;
        dataStore = new EditorDataStore<BaseTestType>("TestAbstractDataStore");
        
        Assert.AreEqual(afterList, dataStore);
    }
    
    // Update is called once per frame
    void Update()
    {
        
    }
}
[TestFixture]
public class TestClass
{
    public string TestString = "TestString";
    public float TestFloat = 2.0f;
    public int TestInt = 1;
    public double TestDouble = 2.0;
    public Color TestColor = Color.blue;
    public Color32 TestColor32 = Color.red;
    public Gradient TestGradient = new Gradient();
    public List<ValueClass> TestListClasses = new List<ValueClass>()
    {
        new ValueClass()
    };

    public Dictionary<int, ValueClass> TestClassDictionary = new Dictionary<int, ValueClass>();

    public TestClass()
    {
        TestClassDictionary.Add(1, new ValueClass());
    }
    

}
[TestFixture]
public class ValueClass
{
    public int a { get; set; } = 2;
    public string b = "testing";
}

[TestFixture]
public abstract class BaseTestType
{
    public string TestString = "TestString";
    public float TestFloat = 2.0f;
    public int TestInt = 1;
    public double TestDouble = 2.0;
    public Color TestColor = Color.blue;
}
[TestFixture]
public class DerrivedTestType : BaseTestType
{
    public Color32 TestColor32 = Color.red;
    public Gradient TestGradient = new Gradient();
}