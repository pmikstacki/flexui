﻿using Flex.Scripts.Core.Inspect.Editor.UIElements;
using Flex.Scripts.Core.Serialize.Storage;
using UnityEditor;
using UnityEngine.UIElements;

namespace Flex.Scripts.Core.Serialize.Editor
{
    [CustomEditor(typeof(StorageScriptableObject<>), true)]
    public class StorageScriptableObjectInspector : UnityEditor.Editor
    {
        public override VisualElement CreateInspectorGUI()
        {
            var s = target.GetType();
            var root = new VisualElement();
            return target.DrawObjectProperties();
            /*((o, info) =>
            {
                Debug.Log(o.GetType().ToString() + " : " + info.Name);
                
                info.SetValue(s, info.GetValue(o));
                serializedObject.ApplyModifiedProperties();

            })*/

        }
    }
}