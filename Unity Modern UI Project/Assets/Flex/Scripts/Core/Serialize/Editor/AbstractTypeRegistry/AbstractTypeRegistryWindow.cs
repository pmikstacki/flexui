﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using Flex.Scripts.Core.Inspect.Editor.UIElements;
using Flex.Scripts.Core.Inspect.Editor.UIElements.IconButton;
using UnityEditor;
using UnityEditor.SearchService;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace Flex.Scripts.Core.Serialize.Editor.AbstractTypeRegistry
{
    public class AbstractTypeRegistryWindow : EditorWindow
    {
        [MenuItem("Flex/Abstract Type Registry")]
        private static void ShowWindow()
        {
            var window = GetWindow<AbstractTypeRegistryWindow>();
            window.titleContent = new GUIContent("Abstract Type Registry");
            window.Show();
        }

        private Dictionary<bool, Type> _types = new Dictionary<bool, Type>();
        
        private void CreateGUI()
        {
            var root = rootVisualElement;
            var addPanel = new Toolbar();
            addPanel.style.flexDirection = FlexDirection.Row;
            var typeField = new ObjectField("Object to add");
            typeField.objectType = typeof(object);
            var iconButton = new IconButton(() =>
            {
                TypesRegistry.RegisterBaseType(typeField.objectType);
            });
            iconButton.SetIcon("Add");
            addPanel.Add(typeField);
            addPanel.Add(iconButton);
            var scrollView = new ScrollView() {name = "TypeList"};
            
            scrollView.verticalScroller.slider.pageSize = 1000;
            root.Add(addPanel);
            root.Add(scrollView);
            RefreshTypes();

        }


        
        private void RefreshTypes()
        {
            var typeList = rootVisualElement.Q("TypeList");
            typeList.Clear();
            var visParent = new VisualElement();
            
            foreach (var keyValuePair in TypesRegistry.Types)
            {
                var field = new VisualElement();
                field.style.flexDirection = FlexDirection.Row;
                field.Add(new Label(keyValuePair.Name));
                field.Add(new IconButton(() => TypesRegistry.UnregisterBaseType(keyValuePair)) {image = Resources.Load<Texture>("Icons/DeleteIcon")});
                
                visParent.Add(field);
            }
            typeList.Add(visParent);

        }
    }
}