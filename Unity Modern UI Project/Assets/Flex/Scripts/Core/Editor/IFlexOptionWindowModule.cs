﻿using UnityEngine;
using UnityEngine.UIElements;

namespace Flex.Scripts.Core.Editor
{
    public interface IFlexOptionWindowModule
    {
        public int order { get; }
        public string Name { get; }
        public VisualElement RenderModule();    
        public Texture Icon { get; }
    }
}