using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Flex.Scripts.Core.Inspect.Editor.Content;
using Flex.Scripts.Core.Inspect.Editor.Extensions;
using Flex.Scripts.Core.Inspect.Editor.UIElements;
using Flex.Scripts.Core.Inspect.Editor.UIElements.ButtonWithIcon;
using Flex.Scripts.Core.Inspect.Editor.UIElements.List;
using Flex.Scripts.Core.Inspect.Editor.UIElements.Tabs;
using UnityEditor;
using UnityEditor.IMGUI.Controls;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace Flex.Scripts.Core.Editor
{ 
    public class FlexUIEditorWindow : EditorWindow
    {
    
        [MenuItem("FlexUI/Manager")]
        public static void ShowWindow()
        {
            FlexUIEditorWindow wnd = GetWindow<FlexUIEditorWindow>();
            wnd.titleContent = new GUIContent("FlexUI Manager");
        }

        public void CreateGUI()
        {
            VisualElement root = rootVisualElement;
            root.AddStyleSheet("FlexUIEditorWindow.uss");
            root.AddToClassList("Main");
            root.style.flexDirection = FlexDirection.Row;
            VisualElement contentHost = new VisualElement();
            contentHost.AddToClassList("contentHost");

            ScrollView scrollView = new ScrollView();

            VisualElement sidebar = new VisualElement();

            VisualElement logo = new VisualElement();
            var logoImage = new UnityEngine.UIElements.Image();
            logoImage.image = Resources.Load<Texture>("Icons/Logo");
            logo.Add(logoImage);
            logo.AddToClassList("Logo");

            var logoTextGroup = new VisualElement();
            var logoTitleLabel = new Label("Flex UI");
            logoTitleLabel.AddToClassList("Title");
            var logoLabel = new Label("Flexible Interface \nFramework");
            logoLabel.AddToClassList("LogoLabel");

            logoTextGroup.Add(logoTitleLabel);
            logoTextGroup.Add(logoLabel);
            logo.Add(logoTextGroup);
            sidebar.Add(logo);

            sidebar.AddToClassList("stretch");
            Type lookupType = typeof(IFlexOptionWindowModule);
            IEnumerable<Type> lookupTypes =
                AppDomain.CurrentDomain.GetAssemblies().SelectMany(x => x.GetTypes().Where(
                    t => lookupType.IsAssignableFrom(t) && !t.IsInterface));
            List<IFlexOptionWindowModule> modules = new List<IFlexOptionWindowModule>();
            foreach (var installedModule in lookupTypes)
            {
                var installedModuleInstance = Activator.CreateInstance(installedModule) as IFlexOptionWindowModule;
                modules.Add(installedModuleInstance);
            }

            modules = modules.OrderBy(x => x.order).ToList();

            foreach (var installedModuleInstance in modules)
            {
                var moduleButton = new ButtonWithIcon(() =>
                {
                    contentHost.ClearChildren();
                    if (installedModuleInstance != null && contentHost.Q<VisualElement>("ModuleOptions") != null)
                        contentHost.Remove(contentHost.Q<VisualElement>("ModuleOptions"));
                    if (installedModuleInstance != null)
                    {
                        var element = installedModuleInstance.RenderModule();
                        element.AddStyleSheet("FlexUIEditorWindow.uss");
                        element.AddToClassList("Main");
                        contentHost.Add(element);
                    }
                }, installedModuleInstance.Icon, installedModuleInstance.Name);
                moduleButton.AddToClassList("ModuleButton");
                sidebar.Add(moduleButton);
            }

            sidebar.style.width = 170;
            scrollView.Add(sidebar);
            root.Add(scrollView);
            root.Add(contentHost);
        }
    }

    public class ProxySelect
    {
        public string Name { get; private set; }
        public IListProxy Proxy { get; private set; }
        public Type ItemType { get; private set; }
        public bool AllowDerived { get; private set; }
        public IList List { get; private set; }

        public ProxySelect(string name, IListProxy proxy, Type itemType, bool allowDerived, IList list)
        {
            Name = name;
            Proxy = proxy;
            ItemType = itemType;
            AllowDerived = allowDerived;
            List = list;
        }
    }
}