﻿using Flex.Scripts.Core.Inspect.Editor.Extensions;
using UnityEngine;
using UnityEngine.UIElements;

namespace Flex.Scripts.Core.Editor.OptionModules.AboutModule
{
    public class FlexOptionWindowAbout : IFlexOptionWindowModule
    {
        public int order => 100;
        public string Name => "About";
        public VisualElement RenderModule()
        {
            var root = new VisualElement();
            root.AddStyleSheet("AboutStyle.uss");
            root.name = "ModuleOptions";
            var scrollView = new ScrollView();
            scrollView.AddToClassList("RootScrollView");

            var logoContainer = new VisualElement();
            logoContainer.AddToClassList("LogoContainer");
            logoContainer.style.flexDirection = FlexDirection.Row;
            var logoImage = new UnityEngine.UIElements.Image()
            {
                image = Resources.Load<Texture>("Icons/Logo")
            };
            logoImage.AddToClassList("LogoImage");

            logoContainer.Add(logoImage);
            var logoTextContainer = new VisualElement();
            
            var titleLabel = new Label("Flex UI");
            titleLabel.AddToClassList("TitleLabel");
            var subtitleLabel = new Label("Flexible Interface Framework \n By Astro Games PL \n Alpha 0.1");
            subtitleLabel.AddToClassList("LogoLabel");
            logoTextContainer.Add(titleLabel);
            logoTextContainer.Add(subtitleLabel);
            logoContainer.Add(logoTextContainer);
            scrollView.Add(logoContainer);
            
            
            root.Add(scrollView);
            return root; 
        }

        public Texture Icon => Resources.Load<Texture>("Icons/AboutIcon");
    }
}