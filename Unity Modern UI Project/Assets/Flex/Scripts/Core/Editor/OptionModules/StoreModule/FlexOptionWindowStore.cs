﻿using UnityEngine;
using UnityEngine.UIElements;

namespace Flex.Scripts.Core.Editor.OptionModules.StoreModule
{
    public class FlexOptionWindowStore : IFlexOptionWindowModule
    {
        public int order => 99;
        public string Name => "Store";
        public VisualElement RenderModule()
        {
            var root = new VisualElement();
            root.Add(new Label("Welcome to FlexUI Store!"));
            root.name = "ModuleOptions";
            return root;
        }

        public Texture Icon => Resources.Load<Texture>("Icons/CartIcon");
    }
}