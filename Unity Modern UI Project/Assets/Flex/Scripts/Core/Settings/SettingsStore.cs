﻿using System.Collections.Generic;
using System.Linq;
using Flex.Scripts.Core;
using Flex.Scripts.Core.Serialize.Storage;

namespace Flex.Core.Settings.Flex.Scripts.Core.Settings
{
    public static class SettingsStore
    {
        private static readonly EditorDataStore<Setting> settingsStore;
        static SettingsStore()
        {
            settingsStore = new EditorDataStore<Setting>("FlexUISettings");
        }

        public static List<Setting> GetAllSettings()
        {
            return settingsStore.GetAll();
        }
        
        public static object GetSetting(string settingKey)
        {
            var setting = settingsStore.First(x => x.SettingName == settingKey);
            if (setting != null)
            {
                return setting.SettingValue;
            }
            else
            {
                return new Setting();
            }
        }

        public static void RemoveSetting(string settingKey)
        {
            settingsStore.RemoveAll(x => x.SettingName == settingKey);
            settingsStore.Save();
        }
        
        public static void SetSetting(string settingKey, object target)
        {
            if (settingsStore.All(x => x.SettingName != settingKey))
            {
                settingsStore.Add(new Setting()
                {
                    ShowInSettings = true,
                    SettingPath = "/",
                    SettingName = settingKey,
                    SettingValue = target
                });
            }
            else
            {
                settingsStore[settingsStore.IndexOf(settingsStore.First(x => x.SettingName == settingKey))].SettingValue = target;
            }
            
            settingsStore.Save();
        }
        
        public static void SetSetting(string settingKey, object target, string settingPath)
        {
            if (settingsStore.All(x => x.SettingName != settingKey))
            {
                settingsStore.Add(new Setting()
                {
                    ShowInSettings = true,
                    SettingPath = settingPath,
                    SettingName = settingKey,
                    SettingValue = target
                });
            }
            else
            {
                settingsStore[settingsStore.IndexOf(settingsStore.First(x => x.SettingName == settingKey))].SettingValue = target;
            }
            
            settingsStore.Save();
        }

        public static void SetSetting(bool showInSettings, string settingKey, object target)
        {
            if (settingsStore.All(x => x.SettingName != settingKey))
            {
                settingsStore.Add(new Setting()
                {
                    ShowInSettings = showInSettings,
                    SettingPath = null,
                    SettingName = settingKey,
                    SettingValue = target
                });
            }
            else
            {
                settingsStore[settingsStore.IndexOf(settingsStore.First(x => x.SettingName == settingKey))]
                    .SettingValue = target;
            }

            settingsStore.Save();
        }

        public static void SetSetting(bool showInSettings, string settingPath, string settingKey, object target)
        {
            if (settingsStore.All(x => x.SettingName != settingKey))
            {
                settingsStore.Add(new Setting()
                {
                    ShowInSettings = showInSettings,
                    SettingPath = settingPath,
                    SettingName = settingKey,
                    SettingValue = target
                });
            }
            else
            {
                settingsStore[settingsStore.IndexOf(settingsStore.First(x => x.SettingName == settingKey))].SettingValue = target;
            }
            settingsStore.Save();
        }
    }
}