﻿using Flex.Scripts.Core;

namespace Flex.Core.Settings.Flex.Scripts.Core.Settings
{
    public class Setting
    {
        public string SettingName { get; set; }
        public object SettingValue { get; set; }
        public bool ShowInSettings { get; set; }
        
        public string SettingPath { get; set; }
        
    }
}