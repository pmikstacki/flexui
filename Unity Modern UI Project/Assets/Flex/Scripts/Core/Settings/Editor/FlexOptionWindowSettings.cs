﻿using System;
using System.Collections;
using Flex.Core.Settings.Flex.Scripts.Core.Settings;
using Flex.Scripts.Core.Editor;
using Flex.Scripts.Core.Inspect.Editor.Extensions;
using Flex.Scripts.Core.Inspect.Editor.UIElements;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace Flex.Scripts.Core.Settings.Editor
{
    public class FlexOptionWindowSettings : IFlexOptionWindowModule
    {
        public int order => 98;
        public string Name => "Settings";

        public VisualElement RenderModule()
        {
            var root = new VisualElement();
            root.AddStyleSheet("SettingsStyle.uss");
            root.DrawHeader("Settings", "https://flexuidocs.astrogames.pl/#/Modules/Settings/Settings");
            root.name = "ModuleOptions";
            var scrollView = new ScrollView();
            scrollView.Add(RenderSettings());
            root.Add(scrollView);
            return root; 
        }

        public Texture Icon => Resources.Load<Texture>("Icons/SettingsIcon");


        private VisualElement RenderSettings()
        {
            var root = new VisualElement();
            foreach (var element in SettingsStore.GetAllSettings())
            {
                if(element.ShowInSettings)
                    root.Add(RenderSetting(element));
            }

            return root;

        }

        private VisualElement RenderSetting(Setting setting)
        {
            if (setting.SettingValue is float floatVal)
            {
                var floatField = new FloatField(setting.SettingName);
                floatField.value = floatVal;
                floatField.RegisterValueChangedCallback(e =>
                {
                    SettingsStore.SetSetting(setting.SettingName, e.newValue);
                });
                return floatField;
            }

            if (setting.SettingValue is int intVal)
            {
                var integerField = new IntegerField(setting.SettingName);
                integerField.value = intVal;
                integerField.RegisterValueChangedCallback(e =>
                {
                    SettingsStore.SetSetting(setting.SettingName, e.newValue);
                });
                return integerField;
            }

            if (setting.SettingValue is string strVal)
            {
                var textField = new TextField(setting.SettingName);
                textField.value = strVal;
                textField.RegisterValueChangedCallback(e =>
                {
                    SettingsStore.SetSetting(setting.SettingName, e.newValue);
                });
                return textField;
            }

            if (setting.SettingValue is bool boolVal)
            {
                var toggle = new Toggle(setting.SettingName);
                toggle.value = boolVal;
                toggle.RegisterValueChangedCallback(e =>
                {
                    SettingsStore.SetSetting(setting.SettingName, e.newValue);
                });
                return toggle;
            }
            if (setting.SettingValue is Vector2 vector2)
            {
                var vector2Field = new Vector2Field(setting.SettingName);
                vector2Field.value = vector2;
                vector2Field.RegisterValueChangedCallback(e =>
                {
                    SettingsStore.SetSetting(setting.SettingName, e.newValue);
                });
                return vector2Field;
            }
            if (setting.SettingValue is Vector3 vector3)
            {
                var vector3Field = new Vector3Field(setting.SettingName);
                vector3Field.value = vector3;
                vector3Field.RegisterValueChangedCallback(e =>
                {
                    SettingsStore.SetSetting(setting.SettingName, e.newValue);
                });
                return vector3Field;
            }
            if (setting.SettingValue is Enum enumVal)
            {
                var enumField = new EnumField(setting.SettingName, enumVal);
                enumField.value = enumVal;
                enumField.RegisterValueChangedCallback(e =>
                {
                    SettingsStore.SetSetting(setting.SettingName, e.newValue);
                });
                return enumField;
            }
            if (setting.SettingValue is Color32 color32)
            {
                var color32Field = new ColorField(setting.SettingName);
                color32Field.value = color32;
                color32Field.RegisterValueChangedCallback(e =>
                {
                    SettingsStore.SetSetting(setting.SettingName, e.newValue);
                });
                return color32Field;
            }
            if (setting.SettingValue is Color color)
            {
                var colorField = new ColorField(setting.SettingName);
                colorField.value = color;
                colorField.RegisterValueChangedCallback(e =>
                {
                    SettingsStore.SetSetting(setting.SettingName, e.newValue);
                });
                return colorField;
            }
            if (setting.SettingValue is Gradient gradient)
            {
                var gradientField = new GradientField(setting.SettingName);
                gradientField.value = gradient;
                gradientField.RegisterValueChangedCallback(e =>
                {
                    SettingsStore.SetSetting(setting.SettingName, e.newValue);
                });
                return gradientField;
            }
            if (setting.SettingValue is IList list)
            {
               /*var enumField = new ListField(setting.SettingName, enumVal);
                enumField.value = enumVal;
                enumField.RegisterValueChangedCallback(e =>
                {
                    SettingsStore.SetSetting(setting.SettingName, e.newValue);
                });
                return enumField;*/
               //return null;
            }
            return new Label($"Type {setting.SettingValue.GetType().ToString()} is not supported.");
        }


    }
}