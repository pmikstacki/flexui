﻿namespace Flex.Scripts.Core.Inspect.Runtime.Attributes.Drawers
{
	public class ReferenceAttribute : PropertyTraitAttribute
	{
		public bool IsCollapsable = true;

		public ReferenceAttribute() : base(ControlPhase, 0)
		{
		}
	}
}
