﻿namespace Flex.Scripts.Core.Inspect.Runtime.Attributes.Traits
{
	public class MultilineAttribute : PropertyTraitAttribute
	{
		public MultilineAttribute() : base(FieldPhase, 0)
		{
		}
	}
}