﻿namespace Flex.Scripts.Core.Inspect.Runtime.Attributes.Drawers
{
	public class FrameAttribute : PropertyTraitAttribute
	{
		public bool IsCollapsable = true;

		public FrameAttribute() : base(ControlPhase, 0)
		{
		}
	}
}
