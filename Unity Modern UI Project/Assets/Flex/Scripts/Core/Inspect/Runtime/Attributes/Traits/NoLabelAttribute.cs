﻿namespace Flex.Scripts.Core.Inspect.Runtime.Attributes.Traits
{
	public class NoLabelAttribute : PropertyTraitAttribute
	{
		public NoLabelAttribute() : base(PerContainerPhase, 0)
		{
		}
	}
}