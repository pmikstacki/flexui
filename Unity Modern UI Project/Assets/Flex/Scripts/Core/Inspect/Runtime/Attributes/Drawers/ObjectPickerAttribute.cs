﻿namespace Flex.Scripts.Core.Inspect.Runtime.Attributes.Drawers
{
	public class ObjectPickerAttribute : PropertyTraitAttribute
	{
		public ObjectPickerAttribute() : base(ControlPhase, 0)
		{
		}
	}
}
