﻿using System;

namespace Flex.Scripts.Core.Inspect.Runtime.Attributes.Drawers
{
	public class TypePickerAttribute : PropertyTraitAttribute
	{
		public Type BaseType { get; private set; }
		public bool ShowAbstract { get; private set; }

		public TypePickerAttribute(Type baseType, bool showAbstract = false) : base(ControlPhase, 0)
		{
			BaseType = baseType;
			ShowAbstract = showAbstract;
		}
	}
}
