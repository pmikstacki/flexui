﻿namespace Flex.Scripts.Core.Inspect.Runtime.Attributes.Traits
{
	public class DelayAttribute : PropertyTraitAttribute
	{
		public DelayAttribute() : base(FieldPhase, 0)
		{
		}
	}
}