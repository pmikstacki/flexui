﻿namespace Flex.Scripts.Core.Inspect.Runtime.Attributes.Drawers
{
	public class EulerAttribute : PropertyTraitAttribute
	{
		public EulerAttribute() : base(ControlPhase, 0)
		{
		}
	}
}