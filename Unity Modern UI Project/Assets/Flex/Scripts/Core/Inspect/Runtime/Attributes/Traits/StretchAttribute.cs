﻿namespace Flex.Scripts.Core.Inspect.Runtime.Attributes.Traits
{
	public class StretchAttribute : PropertyTraitAttribute
	{
		public StretchAttribute() : base(FieldPhase, 0)
		{
		}
	}
}