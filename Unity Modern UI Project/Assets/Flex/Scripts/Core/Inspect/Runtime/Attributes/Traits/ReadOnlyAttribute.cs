﻿namespace Flex.Scripts.Core.Inspect.Runtime.Attributes.Traits
{
	public class ReadOnlyAttribute : PropertyTraitAttribute
	{
		public ReadOnlyAttribute() : base(PerContainerPhase, 10)
		{
		}
	}
}