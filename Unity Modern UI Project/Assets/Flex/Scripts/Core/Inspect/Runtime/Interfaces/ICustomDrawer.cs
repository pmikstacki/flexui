﻿using UnityEngine.UIElements;

namespace Flex.Scripts.Core.Inspect.Runtime.Interfaces
{
    public interface ICustomDrawer
    {
        public VisualElement DrawProperty();
    }
}