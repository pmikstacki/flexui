﻿using Flex.Scripts.Core.Inspect.Runtime.Attributes.Drawers;
using UnityEditor;
using UnityEngine.UIElements;

namespace Flex.Scripts.Core.Inspect.Editor.UIElements.Inline
{
	[CustomPropertyDrawer(typeof(InlineAttribute))]
	public class InlineDrawer : PropertyDrawer
	{
		public const string Stylesheet = "InlineStyle.uss";
		public const string UssClassName = "pirho-inline";
		public const string LabelUssClassName = UssClassName + "__label";
		public const string ChildrenUssClassName = UssClassName + "__children";

		public override VisualElement CreatePropertyGUI(SerializedProperty property)
		{
			var showMemberLabels = (attribute as InlineAttribute).ShowMemberLabels;
			return new InlineField(property, showMemberLabels);
		}
	}
}