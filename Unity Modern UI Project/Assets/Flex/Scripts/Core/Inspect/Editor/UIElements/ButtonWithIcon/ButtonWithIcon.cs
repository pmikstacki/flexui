﻿using System;
using Flex.Scripts.Core.Inspect.Editor.Extensions;
using UnityEngine;
using UnityEngine.UIElements;

namespace Flex.Scripts.Core.Inspect.Editor.UIElements.ButtonWithIcon
{
    public class ButtonWithIcon : UnityEngine.UIElements.Button
    {
        public ButtonWithIcon(Action onClick, Texture iconTexture, string label) : base(onClick)
        {
            
            var img = new Image()
            {
                image = iconTexture
            };
            img.AddToClassList("Icon");
            this.Add(img); 
            this.text = label;
            this.AddStyleSheet("ButtonWithIconStyle.uss");
            this.AddToClassList("ButtonWithIcon");
           
        }
    }
}