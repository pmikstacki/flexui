﻿using System;
using System.Linq;
using System.Reflection;
using Flex.Scripts.Core.Inspect.Editor.Attributes;
using UnityEngine;
using UnityEngine.UIElements;
using Fasterflect;
using Flex.Scripts.Core.Inspect.Editor.Extensions;
using Flex.Scripts.Core.Inspect.Runtime.Extensions;
using UnityEditor;

namespace Flex.Scripts.Core.Inspect.Editor.UIElements
{
    public static class InspectorExtensions
    {
        public static void ClearChildren(this VisualElement element)
        {
            for(int i =0;i<element.childCount;i++)
            {
                var child = element.Children().GetEnumerator().Current;
                if (child != null) element.Remove(child);
                element.Children().GetEnumerator().MoveNext();
            }
        }
      
        public static void DrawHeader(this VisualElement element, string headerText, string helpLink)
        {
            var header = new VisualElement();
            header.AddToClassList("HeaderContainer");
            var title = new Label(headerText);
            title.AddToClassList("Title");
            header.Add(title);

            var themesHelp = new IconButton.IconButton(() =>
            {
                Application.OpenURL(helpLink);
            });
            themesHelp.image = Resources.Load<Texture>("Icons/AboutIcon");
            themesHelp.AddToClassList("Info");
            themesHelp.tooltip = "Get Help!";
            header.Add(themesHelp);
            element.Add(header);
        }

        public static VisualElement DrawObjectProperties(this object target)
        {
            var root = new VisualElement();
            root.Add(new Label(target.GetType().Name.ToProperCase()));
            var type = target.GetType();
            var fields = type.GetFields();
            var properties = type.GetProperties(); 
            type.PropertiesWith(Flags.Public | Flags.InstancePublic, typeof(ShowAttribute)).OrderBy(x => x.GetCustomAttribute<ShowAttribute>().Order).ToList().ForEach(member =>
            {
                if (type.GetCustomAttribute<ShowAttribute>().Show == true)
                {
                    var t = new TypeField.TypeField(member.GetValue(target), member.Name,
                        o => member.SetValue(target, o));
                    root.Add(t);
                }
            });
            type.PropertiesWith(Flags.Public | Flags.InstancePublic).ToList().ForEach(member => 
            {
                var t = new TypeField.TypeField(member.GetValue(target), member.Name, o => member.SetValue(target, o));
                root.Add(t); 
            });
            return root;
        }
        
        public static VisualElement DrawObjectFields(this object target, Action<object, FieldInfo> valueChanged)
        {
            var root = new VisualElement();
            var type = target.GetType();
            var fields = type.GetFields();
            //Fields with ShowAttribute
            type.FieldsWith(Flags.Public | Flags.InstancePublic, typeof(ShowAttribute)).OrderBy(x => x.GetCustomAttribute<ShowAttribute>().Order).ToList().ForEach(member => 
            {
                if (type.GetCustomAttribute<ShowAttribute>().Show == true)
                {
                    var t = new TypeField.TypeField(member.GetValue(target), member.Name,
                        o => member.SetValue(target, o));
                    root.Add(t);
                }
            });
            type.FieldsWith(Flags.Public | Flags.InstancePublic ).ToList().ForEach(member =>
            {
                var t = new TypeField.TypeField(member.GetValue(target), member.Name, o => member.SetValue(target, o));
                root.Add(t);
            });
            return root;

        }
        public static VisualElement DrawObjectProperties(this object target, Action<object, PropertyInfo> valueChanged)
        {
            var root = new VisualElement();
            var type = target.GetType();
            var properties = type.GetProperties(); 
            //Properties with Show Attribute
            foreach (var property in properties.Where(x => x.GetCustomAttribute<ShowAttribute>() != null &&  x.GetCustomAttribute<ShowAttribute>().Show).ToList().OrderBy(x => x.GetCustomAttribute<ShowAttribute>().Order))
            {
                var t = new TypeField.TypeField(property.GetValue(target), property.Name, o => valueChanged(o, property));
                root.Add(t);           
            }
            foreach (var property in properties.Where(x => x.GetCustomAttribute<ShowAttribute>() == null))
            {
                var t = new TypeField.TypeField(property.GetValue(target), property.Name, o => valueChanged(o, property));
                root.Add(t);                     
            }
            
            return root;

        }

        public static PropertyDrawer GetIMGUIPropertyDrawerOfType(this Type drawerOwner)
        {
            return Activator.CreateInstance(AppDomain.CurrentDomain.GetAssemblies().SelectMany(x => x.GetTypes())
                .Where(x => x.HasAttribute(typeof(CustomPropertyDrawer))).First(x =>
                    (x.GetAttribute<CustomPropertyDrawer>().GetFieldValue("m_Type") as Type) == drawerOwner)) as PropertyDrawer;
        }

    }
}