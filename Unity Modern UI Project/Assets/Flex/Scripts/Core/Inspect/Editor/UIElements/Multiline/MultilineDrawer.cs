﻿using Flex.Scripts.Core.Inspect.Editor.Extensions;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;
namespace Flex.Scripts.Core.Inspect.Editor.UIElements.Multiline
{
	[CustomPropertyDrawer(typeof(MultilineAttribute))]
	class MultilineDrawer : PropertyDrawer
	{
		private const string _invalidDrawerWarning = "(PUMDID) invalid drawer for MultilineAttribute on field {0}: the element does not have a TextField";

		public override VisualElement CreatePropertyGUI(SerializedProperty property)
		{
			var element = this.CreateNextElement(property);
			var input = element.Q<TextField>();

			if (input != null)
				input.multiline = true;
			else
				Debug.LogWarningFormat(_invalidDrawerWarning, property.propertyPath);

			return element;
		}
	}
}