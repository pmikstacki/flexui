﻿using System;
using Flex.Scripts.Core.Inspect.Editor.Extensions;
using UnityEngine;
using UnityEngine.UIElements;

namespace Flex.Scripts.Core.Inspect.Editor.UIElements.Dialogs
{
    public class StringDialog
    {
        private VisualElement _container = new VisualElement();
        public Action<string> OnResult;
        
        public void Show(VisualElement root, string Message)
        {
            var absoluteRoot = root.GetRootElement();
            Debug.Log(absoluteRoot.name);
            var overlay = new VisualElement();
            overlay.AddToClassList("Overlay");
            overlay.experimental.animation.Scale(1, 1);
            _container.Add(overlay);
            _container.AddToClassList("Container");
            _container.AddStyleSheet("DialogStyle.uss");
            var dialogWindow = new VisualElement();
            dialogWindow.AddToClassList("DialogWindow");
            var dialogueWindowHeader = new Label(Message);
            dialogueWindowHeader.AddToClassList("Header");
            dialogWindow.Add(dialogueWindowHeader);
            
            var textField = new TextField();
            var okButton = new UnityEngine.UIElements.Button(() =>
            {
                OnResult.Invoke(textField.text);
                absoluteRoot.Q(classes: "Main").Remove(_container); 
            });
            okButton.text = "OK";
            var cancelButton = new UnityEngine.UIElements.Button(() =>
            {
                absoluteRoot.Q(classes: "Main").Remove(_container);
            });
            cancelButton.text = "Cancel";

            var buttonsContainer = new VisualElement();
            buttonsContainer.AddToClassList("Buttons");
            buttonsContainer.Add(cancelButton);
            buttonsContainer.Add(okButton);
            buttonsContainer.style.flexDirection = FlexDirection.Row;
            dialogWindow.Add(textField);
            dialogWindow.Add(buttonsContainer);
            _container.Add(dialogWindow);
            absoluteRoot.Q(classes: "Main").Add(_container);
        }
    }
}