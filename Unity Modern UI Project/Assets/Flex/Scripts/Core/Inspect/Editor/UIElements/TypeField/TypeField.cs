﻿using System;
using System.Collections;
using System.Reflection;
using Fasterflect;
using Flex.Scripts.Core.Inspect.Editor.Extensions;
using Flex.Scripts.Core.Inspect.Editor.UIElements.List;
using Flex.Scripts.Core.Inspect.Editor.UIElements.Slider;
using Flex.Scripts.Core.Inspect.Runtime.Attributes.Drawers;
using Flex.Scripts.Core.Inspect.Runtime.Extensions;
using Flex.Scripts.Core.Inspect.Runtime.Interfaces;
using UnityEditor.UI;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace Flex.Scripts.Core.Inspect.Editor.UIElements.TypeField
{
    public class TypeField : VisualElement
    {
        public TypeField(object target, string label, Action<object> valueChanged)
        {
            var supplied = false;
            var properLabel = label.ToProperCase();
            if (target is float floatVal)
            {
                var floatField = new FloatField(properLabel);
                    floatField.value = floatVal;
                    floatField.RegisterValueChangedCallback(e =>
                    {
                        floatVal = e.newValue;
                        valueChanged(e.newValue);
                    });
                    contentContainer.Add(floatField);
                    supplied = true;
            }

           
            
            if (target is int intVal)
            {
                var integerField = new IntegerField(properLabel);
                integerField.value = intVal;
                integerField.RegisterValueChangedCallback(e =>
                {

                    intVal = e.newValue;
                    valueChanged(e.newValue);

                });
                contentContainer.Add(integerField);
                supplied = true;

            }

            if (target is string strVal)
            {
                var textField = new TextField(properLabel);
                textField.value = strVal;
                textField.RegisterValueChangedCallback(e =>
                {
                    strVal = e.newValue;
                    valueChanged(e.newValue);

                });
                contentContainer.Add(textField);
                supplied = true;

            }

            if (target is bool boolVal)
            {
                var toggle = new Toggle(properLabel);
                toggle.value = boolVal;
                toggle.RegisterValueChangedCallback(e =>
                {
                    boolVal = e.newValue;
                    valueChanged(e.newValue);

                });
                contentContainer.Add(toggle);
                supplied = true;

            }
            
            if (target is Vector2 vector2)
            {
                var vector2Field = new Vector2Field(properLabel);
                vector2Field.value = vector2;
                vector2Field.RegisterValueChangedCallback(e =>
                {
                    vector2 = e.newValue;
                    valueChanged(e.newValue);

                });
                contentContainer.Add(vector2Field);
                supplied = true;

            }
            
            if (target is Vector3 vector3)
            {
                var vector3Field = new Vector3Field(properLabel);
                vector3Field.value = vector3;
                vector3Field.RegisterValueChangedCallback(e =>
                {
                    vector3 = e.newValue;
                    valueChanged(e.newValue);

                });
                contentContainer.Add(vector3Field);
                supplied = true;

            }
            
            if (target is Enum enumVal)
            {
                var enumField = new EnumField(properLabel, enumVal);
                enumField.value = enumVal;
                enumField.RegisterValueChangedCallback(e =>
                {
                    enumVal = e.newValue;
                    valueChanged(e.newValue);

                });
                contentContainer.Add(enumField);
                supplied = true;


            }
            
            if (target is Color32 color32)
            {
                var color32Field = new ColorField(properLabel);
                color32Field.value = new Color((float) color32.r/255, (float) color32.g/255, (float) color32.b/255, (float) color32.a/255) ;
                color32Field.RegisterValueChangedCallback(e =>
                {
                    color32.a = (byte) (e.newValue.a * 255);
                    color32.r = (byte) (e.newValue.r * 255);
                    color32.g = (byte) (e.newValue.g * 255);
                    color32.b = (byte) (e.newValue.b * 255);
                    valueChanged(color32);

                });
                
                contentContainer.Add(color32Field);
                supplied = true;

            }
            
            if (target is Color color)
            {
                var colorField = new ColorField(properLabel);
                colorField.value = color;
                colorField.RegisterValueChangedCallback(e =>
                {
                    color = e.newValue;
                    valueChanged(e.newValue);

                });
                contentContainer.Add(colorField);
                supplied = true;


            }
            
            if (target is Gradient gradient)
            {
                var gradientField = new GradientField(properLabel);
                gradientField.value = gradient;
                gradientField.RegisterValueChangedCallback(e =>
                {
                    gradient = e.newValue;                    
                    valueChanged(e.newValue);

                });
                contentContainer.Add(gradientField);
                supplied = true;

            }
            
           /* if (target is List<StyleSetter> list)
            {
                var listField = new ListField();
                listField.Label = "Style Setters";
                IListProxy proxy = new ListProxy<StyleSetter>(list, (setters, i) =>
                {
                    var setter = setters[i];
                    return setter.DrawObjectProperties();

                });
                listField.SetProxy(proxy, typeof(StyleSetter), true);
                listField.RegisterValueChangedCallback(e =>
                {
                    valueChanged(list);
                });
                contentContainer.Add(listField);
                supplied = true;

            }*/

            if (target is IList listType)
            {
                var listField = new ListField();
                listField.Label = target.GetType().Name.ToProperCase();
                IListProxy proxy = new ListProxy(listType, (setters, i) =>
                {
                    var setter = setters[i];
                    var root = new VisualElement();
                    root.Add(setter.DrawObjectFields(((o, info) => info.SetValue(target, o))));
                  //  root.Add(setter.DrawObjectProperties(((o, info) => info.SetValue(target, o))));

                    return root;
                });
                if (listType.GetType().IsGenericType && listType.GetType().GetGenericTypeDefinition().IsAbstract)
                {
                    listField.SetProxy(proxy, listType.GetType().GetGenericArguments()[0], true);
                }
                else if(listType.GetType().IsGenericType)
                {
                    listField.SetProxy(proxy, listType.GetType().GetGenericArguments()[0], false);
                }
                else if(listType.GetType().IsAbstract)
                {
                    listField.SetProxy(proxy, listType.GetType().GetElementType(), true);
                }
                else 
                {
                    listField.SetProxy(proxy, listType.GetType().GetElementType(), false);
                }
                listField.RegisterValueChangedCallback(e =>
                {
                    valueChanged(listType);
                });
                contentContainer.Add(listField);
                supplied = true;
            }
            
            if (target is UnityEngine.Object obj)
            {
                var objField = new ObjectField();
                objField.label = obj.GetType().Name.ToProperCase();
                objField.RegisterValueChangedCallback(e =>
                {
                    obj = e.newValue;
                    valueChanged(e.newValue);

                });
                contentContainer.Add(objField);
                supplied = true;

            }
            
            if (target != null && !supplied) 
            {
                if(target.GetType().ImplementsInterface<ICustomDrawer>())
                {
                    var type = target.GetType();
                    Debug.Log($"Found ICustomPropertyDrawer!, Type: {type.Name}");

                    contentContainer.Add(target.InvokeDelegate("DrawProperty", null) as VisualElement);
                }
                if (target.GetType().IsGenericType)
                {
                
                    //Można tak ale lepszym pomysłem jest umożliwienie pisania customowych inspektorów które będą  "Injectowane" w klasę type field. 
                    //czyli będzie obczajać jakiego jest typu i jak jest customowy edytor to renderuje a jak nie to generuje (tak jak unity)
                    //[CustomEditor()]...
                    Debug.Log("Found Generic Type!");
                }

            
                contentContainer.Add(target.DrawObjectFields((o, t) =>
                {
                    t.SetValue(target, o);
                    valueChanged(target);
                }));
                
                contentContainer.Add(target.DrawObjectProperties((o, t) =>
                {
                    t.SetValue(target, o);
                    valueChanged(target); 
                }));
                
                supplied = true;
            }
        }
    }
}