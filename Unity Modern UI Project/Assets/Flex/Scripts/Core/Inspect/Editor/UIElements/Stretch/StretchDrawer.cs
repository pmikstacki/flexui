﻿using Flex.Scripts.Core.Inspect.Editor.Extensions;
using Flex.Scripts.Core.Inspect.Runtime.Attributes.Traits;
using UnityEditor;
using UnityEngine.UIElements;

namespace Flex.Scripts.Core.Inspect.Editor.UIElements.Stretch
{
	[CustomPropertyDrawer(typeof(StretchAttribute))]
	class StretchDrawer : PropertyDrawer
	{
		public const string Stylesheet = "StretchStyle.uss";
		public const string UssClassName = "pirho-stretch";

		public override VisualElement CreatePropertyGUI(SerializedProperty property)
		{
			var element = this.CreateNextElement(property);
			element.AddToClassList(UssClassName);
			element.AddStyleSheet(Stylesheet);

			return element;
		}
	}
}