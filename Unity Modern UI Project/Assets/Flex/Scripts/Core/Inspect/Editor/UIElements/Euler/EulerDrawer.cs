﻿using Flex.Scripts.Core.Inspect.Editor.Extensions;
using Flex.Scripts.Core.Inspect.Runtime.Attributes.Drawers;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

namespace Flex.Scripts.Core.Inspect.Editor.UIElements.Euler
{
	[CustomPropertyDrawer(typeof(EulerAttribute))]
	class EulerDrawer : PropertyDrawer
	{
		private const string _invalidTypeWarning = "(PUEEDIT) invalid type for EulerAttribute on field '{0}': Euler can only be applied to Quaternion fields";

		public override VisualElement CreatePropertyGUI(SerializedProperty property)
		{
			if (property.propertyType == SerializedPropertyType.Quaternion)
				return new EulerField().ConfigureProperty(property);
			else
				Debug.LogWarningFormat(_invalidTypeWarning, property.propertyPath);

			return new FieldContainer.FieldContainer(property.displayName);
		}
	}
}