﻿using Flex.Scripts.Core.Inspect.Editor.Extensions;
using Flex.Scripts.Core.Inspect.Runtime.Attributes.Traits;
using UnityEditor;
using UnityEngine.UIElements;

namespace Flex.Scripts.Core.Inspect.Editor.UIElements.ReadOnly
{
	[CustomPropertyDrawer(typeof(ReadOnlyAttribute))]
	class ReadOnlyDrawer : PropertyDrawer
	{
		public override VisualElement CreatePropertyGUI(SerializedProperty property)
		{
			var element = this.CreateNextElement(property);
			element.SetEnabled(false);
			return element;
		}
	}
}