﻿using System;

namespace Flex.Scripts.Core.Inspect.Editor.Attributes
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public class ShowAttribute : Attribute
    {
        public bool Show { get; } = true;
        public int Order { get; } = -1;
        
        public ShowAttribute(bool show = true)
        {
            Show = show;
        }
        
        public ShowAttribute(bool show = true, int order = -1)
        {
            Show = show;
            Order = order;
        }
    }
    
    
}