﻿using System;
using System.Text.RegularExpressions;
using Flex.Scripts.FlexUI.Animation.Utility;
using UnityEditor;
using UnityEngine;

namespace Flex.Scripts.FlexUI.Animation.Editor
{
    [CustomPropertyDrawer(typeof(Direction))]
    public class DirectionDrawer : PropertyDrawer
    {
        private bool expanded;
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            if (expanded)
                return 120;
            else
                return 20;
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            expanded = EditorGUI.Foldout(new Rect(position.x, position.y, position.width, 20f),expanded, fieldInfo.Name);
            if (expanded)
            {
                
                GUI.Box(
                    new Rect(position.width / 2 - 90, position.y + 20, 140, position.height-20),
                    Regex.Replace(property.FindPropertyRelative("moveDirection").enumNames[property.FindPropertyRelative("moveDirection").enumValueIndex], @"(?<=[A-Za-z])(?=[A-Z][a-z])|(?<=[a-z0-9])(?=[0-9]?[A-Z])", " "));
                EditorGUI.BeginChangeCheck();
                property.FindPropertyRelative("moveDirection").enumValueIndex = GUI.SelectionGrid(new Rect(position.width / 2 - 90 + 2, position.y + 38, 180, position.height-22), property.FindPropertyRelative("moveDirection").enumValueIndex,new String[] {"", "","","", "","","", "",""},3, EditorStyles.radioButton);
                if (EditorGUI.EndChangeCheck())
                {
                    Debug.Log($"{property.FindPropertyRelative("moveDirection").enumValueIndex} : {property.FindPropertyRelative("moveDirection").enumNames[property.FindPropertyRelative("moveDirection").enumValueIndex]}");
                }
            }
        }
    }
}