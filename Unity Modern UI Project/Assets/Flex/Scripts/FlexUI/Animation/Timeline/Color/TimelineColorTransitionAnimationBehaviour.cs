﻿using UnityEngine.Playables;

namespace Flex.Scripts.FlexUI.Animation.Timeline.Color
{
    public class TimelineColorAnimationBehaviour : PlayableBehaviour
    {
        public UnityEngine.Color Color = UnityEngine.Color.black;

    }
}