﻿using UnityEditor.Timeline.Actions;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using UnityEngine.UI;

namespace Flex.Scripts.FlexUI.Animation.Timeline.Color
{
    [TrackClipType(typeof(GraphicColorControlAsset))]
    [TrackBindingType(typeof(Graphic))]
    [MenuEntry("FlexUI/Graphics Color Track", 1)]
    public class GraphicColorTrack : TrackAsset
    {
        public override Playable CreateTrackMixer(PlayableGraph graph, GameObject go, int inputCount) {
            return ScriptPlayable<TimelineColorMixerBehaviour>.Create(graph, inputCount);
        }

        
    }
    
}