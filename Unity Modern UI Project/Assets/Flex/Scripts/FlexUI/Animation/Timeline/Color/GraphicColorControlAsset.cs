﻿using UnityEngine;
using UnityEngine.Playables;

namespace Flex.Scripts.FlexUI.Animation.Timeline.Color
{
    public class GraphicColorControlAsset : PlayableAsset
    { 
        public UnityEngine.Color color = UnityEngine.Color.black;
 
        public override Playable CreatePlayable (PlayableGraph graph, GameObject owner)
        {
            var playable = ScriptPlayable<TimelineColorAnimationBehaviour>.Create(graph);
      
            var lightControlBehaviour = playable.GetBehaviour();
            lightControlBehaviour.Color = color;
 
            return playable;   
        }
    }
}