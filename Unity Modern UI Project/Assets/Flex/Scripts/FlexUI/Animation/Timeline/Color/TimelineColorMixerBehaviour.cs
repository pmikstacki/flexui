﻿using UnityEngine.Playables;
using UnityEngine.UI;

namespace Flex.Scripts.FlexUI.Animation.Timeline.Color
{
    public class TimelineColorMixerBehaviour : PlayableBehaviour
    {
        public override void ProcessFrame(Playable playable, FrameData info, object playerData)
        {
            Graphic trackBinding = playerData as Graphic;
            UnityEngine.Color finalColor = UnityEngine.Color.clear;
            if (!trackBinding)
                return;
 
            int inputCount = playable.GetInputCount (); //get the number of all clips on this track
 
            for (int i = 0; i < inputCount; i++)
            {
                float inputWeight = playable.GetInputWeight(i);
                ScriptPlayable<TimelineColorAnimationBehaviour> inputPlayable = (ScriptPlayable<TimelineColorAnimationBehaviour>)playable.GetInput(i);
                TimelineColorAnimationBehaviour input = inputPlayable.GetBehaviour();
            
                // Use the above variables to process each frame of this playable.
                finalColor += input.Color * inputWeight;
            }

            trackBinding.color = new UnityEngine.Color(finalColor.r, finalColor.r, finalColor.b, finalColor.a);
        }
    }
}