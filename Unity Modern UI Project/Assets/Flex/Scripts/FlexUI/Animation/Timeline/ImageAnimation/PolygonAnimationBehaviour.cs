using System;
using Flex.Scripts.FlexUI.PlayableTimeline.Assets.UnityUIPlayables.Runtime.Shared;
using UnityEngine;

namespace Flex.Scripts.FlexUI.Animation.Timeline.ImageAnimation
{
    [Serializable]
    public class PolygonAnimationBehaviour : AnimationBehaviour
    {
        [SerializeField] private PolygonAnimationValue _startValue;
        [SerializeField] private PolygonAnimationValue _endValue;
        public PolygonAnimationValue StartValue => _startValue;
        public PolygonAnimationValue EndValue => _endValue;
    }
}