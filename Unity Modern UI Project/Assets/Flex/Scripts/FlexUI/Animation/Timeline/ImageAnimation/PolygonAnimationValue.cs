using System;
using Flex.Scripts.FlexUI.PlayableTimeline.Assets.UnityUIPlayables.Runtime.GraphicAnimation;
using Flex.Scripts.FlexUI.UIShapesKit.Assets.ThisOtherThing.UI_Shapes_Kit.Geometry;
using Flex.Scripts.FlexUI.UIShapesKit.Assets.ThisOtherThing.UI_Shapes_Kit.Geometry.ShapeUtils;
using UnityEngine;

namespace Flex.Scripts.FlexUI.Animation.Timeline.ImageAnimation
{
    [Serializable]
    public class PolygonAnimationValue : GraphicAnimationValue
    {
        [SerializeField] private GeoUtils.ShapeProperties shapeProperties;

        public GeoUtils.ShapeProperties ShapeProperties
        {
            get { return this.shapeProperties; }
        }

        [SerializeField] private PointsList.PointListsProperties pointListsProperties;

        public PointsList.PointListsProperties PointListsProperties
        {
            get { return this.pointListsProperties; }
        }

        [SerializeField] private Polygons.PolygonProperties polygonProperties;

        public Polygons.PolygonProperties PolygonProperties
        {
            get { return this.polygonProperties; }
        }

        [SerializeField] private GeoUtils.ShadowsProperties shadowsProperties;

        public GeoUtils.ShadowsProperties ShadowsProperties
        {
            get { return this.shadowsProperties; }
        }

        [SerializeField] private GeoUtils.EdgeGradientData edgeGradientData;

        public GeoUtils.EdgeGradientData EdgeGradientData
        {
            get { return this.edgeGradientData; }
        }

        

    }
}