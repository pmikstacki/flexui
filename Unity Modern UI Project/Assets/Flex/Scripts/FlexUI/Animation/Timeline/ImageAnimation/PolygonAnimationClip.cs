using System;
using Flex.Scripts.FlexUI.PlayableTimeline.Assets.UnityUIPlayables.Runtime.Shared;

namespace Flex.Scripts.FlexUI.Animation.Timeline.ImageAnimation
{
    [Serializable]
    public class PolygonAnimationClip : AnimationTimelineClip<PolygonAnimationBehaviour>
    {
    }
}