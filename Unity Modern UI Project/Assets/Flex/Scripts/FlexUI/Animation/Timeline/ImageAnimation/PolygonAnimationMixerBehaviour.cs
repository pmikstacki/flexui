using Flex.Scripts.FlexUI.PlayableTimeline.Assets.UnityUIPlayables.Runtime.Shared;
using Flex.Scripts.FlexUI.UIShapesKit.Assets.ThisOtherThing.UI_Shapes_Kit.Geometry.Shapes;

namespace Flex.Scripts.FlexUI.Animation.Timeline.ImageAnimation
{
    public class PolygonAnimationMixerBehaviour
        : AnimationMixerBehaviour<Polygon, PolygonAnimationMixer, PolygonAnimationBehaviour>
    {
    }
}