using Flex.Scripts.FlexUI.PlayableTimeline.Assets.UnityUIPlayables.Runtime.Shared;
using Flex.Scripts.FlexUI.UIShapesKit.Assets.ThisOtherThing.UI_Shapes_Kit.Geometry.Shapes;
using UnityEngine.Timeline;

namespace Flex.Scripts.FlexUI.Animation.Timeline.ImageAnimation
{
    [TrackColor(0.1098f, 0.3529f, 0.8392f)]
    [TrackClipType(typeof(PolygonAnimationClip))]
    [TrackBindingType(typeof(Polygon))]
    public class PolygonAnimationTrack
        : AnimationTrack<Polygon, PolygonAnimationMixer, PolygonAnimationMixerBehaviour, PolygonAnimationBehaviour>
    {
    }
}