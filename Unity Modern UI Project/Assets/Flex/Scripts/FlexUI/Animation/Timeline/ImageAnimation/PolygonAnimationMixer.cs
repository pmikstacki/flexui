using System;
using Flex.Scripts.FlexUI.PlayableTimeline.Assets.UnityUIPlayables.Runtime.Shared;
using Flex.Scripts.FlexUI.UIShapesKit.Assets.ThisOtherThing.UI_Shapes_Kit.Geometry.Shapes;

namespace Flex.Scripts.FlexUI.Animation.Timeline.ImageAnimation
{
    public class PolygonAnimationMixer : AnimationMixer<Polygon, PolygonAnimationBehaviour>
    {
        private readonly PolygonValuesMixer _polygonValuesMixer = new PolygonValuesMixer();

        public override void SetupFrame(Polygon binding)
        {
            base.SetupFrame(binding);
            _polygonValuesMixer.SetupFrame();
        }

        public override void Blend(PolygonAnimationBehaviour behaviour, float inputWeight, float progress)
        {
            _polygonValuesMixer.Blend(Tuple.Create(behaviour.StartValue.ShapeProperties, behaviour.StartValue.PolygonProperties, behaviour.StartValue.PointListsProperties), Tuple.Create(behaviour.StartValue.ShapeProperties, behaviour.StartValue.PolygonProperties, behaviour.StartValue.PointListsProperties), inputWeight, progress);
            
        }

        public override void ApplyFrame()
        {
            _polygonValuesMixer.ApplyFrame(Binding);
        }
    }
}