﻿using System;
using System.Linq;
using Flex.Scripts.FlexUI.UIShapesKit.Assets.ThisOtherThing.UI_Shapes_Kit.Geometry;
using Flex.Scripts.FlexUI.UIShapesKit.Assets.ThisOtherThing.UI_Shapes_Kit.Geometry.Shapes;
using Flex.Scripts.FlexUI.UIShapesKit.Assets.ThisOtherThing.UI_Shapes_Kit.Geometry.ShapeUtils;
using UnityEngine;

namespace Flex.Scripts.FlexUI.Animation.Timeline.ImageAnimation
{
    public class PolygonValuesMixer
    {
        private GeoUtils.ShapeProperties _shapeProperties;
        private Polygons.PolygonProperties _polygonProperties;
        private PointsList.PointListsProperties _pointListProperties;
        private float _totalWeight;
        public void SetupFrame()
        {
            _shapeProperties = new GeoUtils.ShapeProperties();
            _polygonProperties = new Polygons.PolygonProperties();
            _pointListProperties = new PointsList.PointListsProperties();
            _totalWeight = 0;
        }

        public void Blend(
            Tuple<GeoUtils.ShapeProperties, Polygons.PolygonProperties, PointsList.PointListsProperties> startValue,
            Tuple<GeoUtils.ShapeProperties, Polygons.PolygonProperties, PointsList.PointListsProperties> endValue,
            float inputWeight, float progress)
        {
            _shapeProperties.FillColor = Color32.Lerp(startValue.Item1.FillColor, endValue.Item1.FillColor,
                progress * inputWeight);
            _polygonProperties.AdjustedCenter =
                Vector2.Lerp(startValue.Item2.AdjustedCenter, endValue.Item2.AdjustedCenter, progress) * inputWeight;
            _polygonProperties.CenterOffset =
                Vector2.Lerp(startValue.Item2.CenterOffset, endValue.Item2.CenterOffset, progress) * inputWeight;
            _polygonProperties.CustomCenter =
                Vector2.Lerp(startValue.Item2.CustomCenter, endValue.Item2.CustomCenter, progress) * inputWeight;
            _polygonProperties.CutoutProperties.Radius = Mathf.Lerp(startValue.Item2.CutoutProperties.Radius,
                endValue.Item2.CutoutProperties.Radius, progress) * inputWeight;
            _polygonProperties.CutoutProperties.Resolution =
                (int) (Mathf.Lerp(startValue.Item2.CutoutProperties.Resolution,
                    endValue.Item2.CutoutProperties.Resolution, progress) * inputWeight);
            _polygonProperties.CutoutProperties.RotationOffset =
                (Mathf.Lerp(startValue.Item2.CutoutProperties.RotationOffset,
                    endValue.Item2.CutoutProperties.RotationOffset, progress) * inputWeight);
            _polygonProperties.CutoutProperties.UnitPositionData.LastBaseAngle =
                (Mathf.Lerp(startValue.Item2.CutoutProperties.UnitPositionData.LastBaseAngle,
                    endValue.Item2.CutoutProperties.UnitPositionData.LastBaseAngle, progress) * inputWeight);
            _polygonProperties.CutoutProperties.UnitPositionData.LastDirection =
                (Mathf.Lerp(startValue.Item2.CutoutProperties.UnitPositionData.LastDirection,
                    endValue.Item2.CutoutProperties.UnitPositionData.LastDirection, progress) * inputWeight);

            if (startValue.Item3.PointListProperties.Count == _pointListProperties.PointListProperties.Count &&
                endValue.Item3.PointListProperties.Count == _pointListProperties.PointListProperties.Count)
            {
                foreach (var point in _pointListProperties.PointListProperties)
                {
                    for (int i = 0; i < point.Positions.Length; i++)
                    {

                        foreach (var startValuePoint in startValue.Item3.PointListProperties)
                        {
                            foreach (var endValuePoint in endValue.Item3.PointListProperties)
                            {
                                if (startValuePoint.Positions.Length <= i && endValuePoint.Positions.Length <= i)
                                    point.Positions[i] =
                                        Vector2.Lerp(startValuePoint.Positions[i], endValuePoint.Positions[i],
                                            progress) *
                                        inputWeight;
                                else
                                {
                                    Debug.LogWarning(
                                        $"The start and end value positions count in point at index {_pointListProperties.PointListProperties.ToList().IndexOf(point)} must be equal or less than the count of the original polygon. ");
                                }
                            }
                        }
                    }

                }
            }
            else
            {
                Debug.LogWarning(
                    "The start and end value positions count must be equal or less than the count of the original polygon. ");

            }

            _totalWeight += inputWeight;
        }

        public void ApplyFrame(Polygon binding)
        {
            if (_totalWeight == 0)
            {
                return;
            }

            //_blendedValue += binding.fillAmount * (1f - _totalWeight);
            binding.ShapeProperties = _shapeProperties;
            binding.PolygonProperties = _polygonProperties;
            binding.PointListsProperties = _pointListProperties;
            binding.ForceMeshUpdate();
        }
    }
}