﻿using System;
using Flex.Scripts.FlexUI.platinioTween.PlatinioTween.Scripts.UI;
using UnityEngine;

namespace Flex.Scripts.FlexUI.Animation.Utility
{
    [Serializable]
    public class Direction
    {
        [SerializeField] private PivotPreset moveDirection;

        public PivotPreset MoveDirection
        {
            get { return this.moveDirection; }
        }

        
    }
}