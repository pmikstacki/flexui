﻿namespace Flex.Scripts.FlexUI.Theming.Editor
{
    /*[CustomPropertyDrawer(typeof(ThemeElement))]
    public class ThemeElementPropertyDrawer : PropertyDrawer
    {
        bool _initialized;
        private string elementName;
        public override void OnGUI(Rect position, SerializedProperty property,
            GUIContent label)
        {
            position.y /= 2;
            elementName = EditorGUI.TextField(new Rect(position.x, position.y + (position.y / 2), position.width, EditorGUI.GetPropertyHeight(property.FindPropertyRelative("styleElementName"))), "Name:", elementName );
            property.FindPropertyRelative("styleElementName").stringValue = elementName;
            
            position.y +=  EditorGUI.GetPropertyHeight(property.FindPropertyRelative("styleElementName")) + 10f;
            Debug.Log(property.serializedObject.GetType());
            if (EditorGUI.DropdownButton(new Rect(position.x, position.y, position.width, 16f), new GUIContent("Add New Setter"), FocusType.Passive))
            {
                StyleSetterDropdown dr = new StyleSetterDropdown(new AdvancedDropdownState());
                dr.Show(new Rect(position.x, position.y, position.width, 100));
            }
        }
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return EditorGUI.GetPropertyHeight(property) + 32f;
        }
        void ArrayGUI(SerializedObject obj,string name)
        {
            int no = obj.FindProperty(name + ".Array.size").intValue;
            EditorGUI.indentLevel = 3;
            int c = EditorGUILayout.IntField("Size", no);
            if (c != no)
                obj.FindProperty(name + ".Array.size").intValue = c;
     
            for (int i=0;i<no;i++) {
                var prop = obj.FindProperty(string.Format("{0}.Array.data[{1}]", name, i));
                EditorGUILayout.PropertyField(prop);
            }
        }

        public static object GetValue(object source, string name)
        {
            if (source == null)
                return null;
     
            var type = source.GetType();
     
            var f = FindFieldInTypeHierarchy(type, name);
     
            if (f == null)
            {
                var p = FindPropertyInTypeHierarchy(type, name);
                if (p == null)
                    return null;
                return p.GetValue(source, null);
            }
            return f.GetValue(source);
        }
     
        public static FieldInfo FindFieldInTypeHierarchy(Type providedType, string fieldName)
        {
            FieldInfo field = providedType.GetField(fieldName, (BindingFlags)(-1));
             
     
            while (field == null && providedType.BaseType != null)
            {
                providedType = providedType.BaseType;
                field = providedType.GetField(fieldName, (BindingFlags)(-1));
            }
     
            return field;
        }
     
        public static PropertyInfo FindPropertyInTypeHierarchy(Type providedType, string propertyName)
        {
            PropertyInfo property = providedType.GetProperty(propertyName, (BindingFlags)(-1));
     
     
            while (property == null && providedType.BaseType != null)
            {
                providedType = providedType.BaseType;
                property = providedType.GetProperty(propertyName, (BindingFlags)(-1));
            }
     
            return property;
        }
    }*/
}