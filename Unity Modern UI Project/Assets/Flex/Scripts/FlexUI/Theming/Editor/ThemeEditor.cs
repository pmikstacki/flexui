﻿using Flex.Scripts.Core.Editor;
using Flex.Scripts.Core.Inspect.Editor.Extensions;
using Flex.Scripts.Core.Inspect.Editor.UIElements;
using Flex.Scripts.Core.Inspect.Editor.UIElements.Dialogs;
using Flex.Scripts.Core.Inspect.Editor.UIElements.IconButton;
using Flex.Scripts.Core.Inspect.Editor.UIElements.List;
using Flex.Scripts.Core.Inspect.Editor.UIElements.Popup;
using Flex.Scripts.FlexUI.Theming.Setters;
using Flex.Scripts.FlexUI.Theming.ThemesSerializer;
using UnityEngine;
using UnityEngine.UIElements;

namespace Flex.Scripts.FlexUI.Theming.Editor 
{
    public class ThemeEditor : IFlexOptionWindowModule
    {
        public int order => 0;
        public string Name => "Themes";

        private StringDialog _dialog;
        private StringDialog _editNameDialog;
        private PopupStringField enumPopup = new PopupStringField("Select Theme");
        private Theme _selectedTheme;
        public ThemeEditor()
        {
            _dialog = new StringDialog();
            _selectedTheme = new Theme();
            _dialog.OnResult += s =>
            {
                if (!ThemesStore.TryCreateNewTheme(s, out _selectedTheme))
                {
                    Debug.LogError($"There can be only one theme with name {s}!");
                }
                enumPopup.SetValues(ThemesStore.GetNames(), ThemesStore.GetNames());
            };
            _editNameDialog = new StringDialog();
            _editNameDialog.OnResult += s =>
            {
                if (!string.IsNullOrEmpty(_selectedTheme.Name))
                {
                    ThemesStore.RenameTheme(_selectedTheme.Name, s);
                    enumPopup.SetValues(ThemesStore.GetNames(), ThemesStore.GetNames());
                }
            };
        }
        
        public VisualElement RenderModule()
        {
            var root = new VisualElement();
            root.AddStyleSheet("ThemeEditorStyle.uss");
            root.DrawHeader("Themes", "https://flexuidocs.astrogames.pl/#/Modules/Themes/Themes");
           
            var themeSelector = new VisualElement();
            themeSelector.AddToClassList("ThemeSelector");
            enumPopup.SetValues(ThemesStore.GetNames(), ThemesStore.GetNames());
            
            themeSelector.Add(enumPopup);
            
            var addButton = new IconButton(() =>
            {
                _dialog.Show(root, "Theme Name");
            });
            
            addButton.image = Resources.Load<Texture>("Icons/NewIcon");
            addButton.tooltip = "Create a new Theme";
            var removeButton = new IconButton(() =>
            {
                ThemesStore.RemoveTheme(enumPopup.value);
                enumPopup.SetValues(ThemesStore.GetNames(), ThemesStore.GetNames());
                enumPopup.value = ThemesStore.GetNames()[0];
            });
            var editButton = new IconButton(() =>
            {
                _editNameDialog.Show(root, "Edit Theme Name");
                enumPopup.SetValues(ThemesStore.GetNames(), ThemesStore.GetNames());
                enumPopup.value = _selectedTheme.Name;
            });
            removeButton.image = Resources.Load<Texture>("Icons/ThrashIcon");
            removeButton.tooltip = "Remove Theme";
            editButton.image = Resources.Load<Texture>("Icons/EditButton");
            editButton.tooltip = "Edit Theme Name";
            var importButton =  new IconButton(() =>
            {
                if (ThemesStore.TryImportThemes())
                {
                    enumPopup.SetValues(ThemesStore.GetNames(), ThemesStore.GetNames());
                    Debug.Log("Themes imported");
                }
                else
                {
                    Debug.LogError("An error occured while importing themes");
                }
            });
            importButton.tooltip = "Import Theme";
            importButton.image =  Resources.Load<Texture>("Icons/ImportIcon");

            var exportButton = new IconButton(() =>
            {
                ThemesStore.ExportTheme(enumPopup.value);
            });
            exportButton.tooltip = "Export Theme";
            
            exportButton.image =  Resources.Load<Texture>("Icons/ExportIcon");
            removeButton.image = Resources.Load<Texture>("Icons/ThrashIcon");
            
            themeSelector.Add(addButton);
            themeSelector.Add(removeButton);
            themeSelector.Add(importButton);
            themeSelector.Add(exportButton);
            themeSelector.Add(editButton); 

            root.Add(themeSelector);
            var scrollView = new ScrollView();
            enumPopup.RegisterValueChangedCallback(evt =>
            {
                _selectedTheme = ThemesStore.GetThemeWithName(evt.newValue);
                scrollView.Clear();
                scrollView.Add(RenderThemeSettings(_selectedTheme));
            });
            
            root.Add(scrollView);
            _selectedTheme = ThemesStore.GetThemeWithName(enumPopup.value);
            scrollView.Clear();
            scrollView.Add(RenderThemeSettings(_selectedTheme));
            
            root.name = "ModuleOptions";
            return root; 
        }

        public VisualElement RenderThemeSettings(Theme theme)
        {
            
            var root = new VisualElement(){name="ListContainer"};
            
            var listField = new ListField();
            listField.Label = "Theme Elements";
            listField.AddHeaderButton(Resources.Load<Texture>("Icons/SaveIcon"), "Save Setters", null, () =>
            {
                
                ThemesStore.SaveTheme(theme); 
            });
            IListProxy proxy = new ListProxy<ThemeElement>(theme.ThemeElements, (themeElements, i) =>
            {
                var themeElement = themeElements[i];
                var localRoot = new VisualElement();
                var themeElemTextField = new TextField("Element Name: ");
                themeElemTextField.value = themeElement.Name;
                themeElemTextField.RegisterValueChangedCallback(e =>
                {
                    themeElement.Name = e.newValue;
                    ThemesStore.SaveTheme(theme);
                });
                var settersField = new ListField();
                settersField.Label = "Style Setters";
                IListProxy proxy = new ListProxy<StyleSetter>(themeElement.StyleSetters, (setters, i) =>
                {
                    var setter = setters[i];
                    var obj = setter.DrawObjectProperties();

                    return obj;
                });
                settersField.SetProxy(proxy, typeof(StyleSetter), true);
                settersField.RegisterValueChangedCallback(e =>
                {
                    ThemesStore.SaveTheme(theme);
                });
                localRoot.Add(themeElemTextField);
                localRoot.Add(settersField);
                return localRoot;
            });
            listField.SetProxy(proxy, typeof(ThemeElement), false);
            listField.RegisterValueChangedCallback(e => ThemesStore.SaveTheme(theme));
            return listField;
        }

        public Texture Icon => Resources.Load<Texture>("Icons/ThemesIcon");
    }
}