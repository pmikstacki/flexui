﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Flex.Scripts.Core.Serialize;
using Flex.Scripts.Core.Serialize.Storage;
using Flex.Scripts.FlexUI.Theming.Setters;
using UnityEditor;
using UnityEngine;
using YamlDotNet.Serialization;

namespace Flex.Scripts.FlexUI.Theming.ThemesSerializer
{
    public static class ThemesStore
    {
        private static ImportableEditorDataStore<Theme> _themesStore;
        
        static ThemesStore()
        {
            TypesRegistry.RegisterBaseType(typeof(StyleSetter));
            _themesStore = new ImportableEditorDataStore<Theme>("FlexUI_Themes");
        }

        public static List<string> GetNames()
        {
            Debug.Log(string.Join(", ", _themesStore.Select(x => x.Name).ToList()));
            return _themesStore.Select(x => x.Name).ToList();
        }
 
        public static void RemoveTheme(string name)
        {
            _themesStore.RemoveAll(x => x.Name == name);
            Save();
        }

        public static void RenameTheme(string name, string newName)
        {
            _themesStore[_themesStore.IndexOf(_themesStore.First(x => x.Name == name))].Name = newName;
            Save();
        }

        public static Theme GetThemeWithName(string name)
        {
            return _themesStore.First(t => t.Name == name);
        }

        public static void SaveTheme(Theme theme)
        {
            if (_themesStore.Contains(theme))
            {
                _themesStore[_themesStore.IndexOf(_themesStore.First(x => x.Name == theme.Name))] = theme;
                Save();
            }
        }
        
        public static bool TryImportThemes()
        {
            var filePath = EditorUtility.OpenFilePanelWithFilters("Select YAML file with Theme", Application.dataPath,
                new string[] {"YAML File", "yml,yaml,YAML"});
            if (!string.IsNullOrEmpty(filePath))
            {
                _themesStore.Import(filePath);
                Save();
                return true;
            }
            else
            {
                return false;
            }
        }

        public static void ExportTheme(string name)
        {
            ExportTheme(_themesStore.First(x => x.Name == name));
        }
        
        public static void ExportTheme(Theme themeToExport)
        {
            List<Theme> themes = new List<Theme>() {themeToExport};
            
            var filePath = EditorUtility.SaveFilePanel("Export Theme", Application.dataPath,
                $"{themeToExport.Name}_exported", "json");
            if (!string.IsNullOrEmpty(filePath))
            {
                try
                {
                    var serializer = new Serializer();
                    File.WriteAllText(filePath, serializer.Serialize(new List<Theme>(){themeToExport}));
                }
                catch (IOException exception)
                {
                    Debug.LogError($"An error occured while exporting theme {themeToExport.Name}. Details: \n {exception.ToString()}");
                }
            }
            else
            {
                Debug.LogWarning($"Theme {themeToExport.Name} export failed, because the sufficient path was not provided.");
            }
        }
        
        public static bool TryCreateNewTheme(string themeName, out Theme theme)
        {
            if (_themesStore.All(x => x.Name != themeName))
            {
                theme = new Theme()
                {
                    Name = themeName
                };
                _themesStore.Add(theme);
                Save();
                return true;
            }
            else
            {
                theme = null;
                return false;
            }
        }        
        
        private static void Save()
        {
            _themesStore.Save();
        }
        
    }
}
