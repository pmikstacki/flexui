using UnityEngine;

namespace Flex.Scripts.FlexUI.Theming
{
    [ExecuteInEditMode]
    public class StyledElement : MonoBehaviour
    {
        [SerializeField] private ThemeElement themeElement;

        public ThemeElement ThemeElement
        {
            get { return this.themeElement; }
            set => themeElement = value;
        }

        public void OnEnable()
        {
            //FlexUIManager.Instance.RegisterStyledElement(this);
        }

        public void ApplyTheme(Theme theme)
        {
            if (!theme.ThemeElements.Contains(themeElement))
                return;


        
            foreach (Component component in transform.GetComponents(typeof(Component)))
            {
                themeElement.StyleSetters.ForEach(setter =>
                {
                    setter.SetStyle(component);
                });
            }
        }
    }
}
