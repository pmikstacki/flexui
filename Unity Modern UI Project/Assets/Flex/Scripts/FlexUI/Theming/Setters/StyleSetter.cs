﻿using System;
using UnityEngine.UIElements;

namespace Flex.Scripts.FlexUI.Theming.Setters
{
    [Serializable]
    public abstract class StyleSetter 
    {
        public abstract void SetStyle(object target);

        public StyleSetter()
        {
            
        }

        public override string ToString()
        {
            return GetType().Name;
        }
    }
}