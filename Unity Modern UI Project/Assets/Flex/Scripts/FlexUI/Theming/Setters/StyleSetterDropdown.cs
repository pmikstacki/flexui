﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Flex.Scripts.Core.Serialize;
using UnityEditor.IMGUI.Controls;

namespace Flex.Scripts.FlexUI.Theming.Setters
{
        public class StyleSetterDropdown : AdvancedDropdown
        {
            public Action<int> selectedItem;

            #region Constructors

            public StyleSetterDropdown(AdvancedDropdownState state) : base(state)
            {
            }

            #endregion

            #region Protected Methods

            protected override AdvancedDropdownItem BuildRoot()
            {
                var root = new AdvancedDropdownItem("Style Setters");
                StyleSetterProvider.StyleSettersDictionary.ToList().ForEach(setter =>
                {
                    var item = new AdvancedDropdownItem(setter.Value.GetType().Name.ToString());
                    item.id = setter.Key;
                    root.AddChild(item);
                });

                return root;
            }

            protected override void ItemSelected(AdvancedDropdownItem item)
            {
                selectedItem.Invoke(item.id);
            }

            #endregion
        }

        public static class StyleSetterProvider
        {
            private static Dictionary<int, StyleSetter> styleSettersDictionary = new Dictionary<int, StyleSetter>();

            public static Dictionary<int, StyleSetter> StyleSettersDictionary
            {
                get
                {
                    RefreshDictionary();
                    return styleSettersDictionary;
                }
            }

            private static void RefreshDictionary()
            {
                styleSettersDictionary.Clear();
                int id = 0;
                foreach (var setter in ReflectiveEnumerator.GetEnumerableOfType<StyleSetter>())
                {
                    styleSettersDictionary.Add(id, (StyleSetter)Activator.CreateInstance(setter));
                    id++;
                }
                //ClearLog();
            }
        }
}