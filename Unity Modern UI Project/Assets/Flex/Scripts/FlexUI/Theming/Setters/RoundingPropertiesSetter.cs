﻿using System;
using Flex.Scripts.Core.Inspect.Editor.UIElements;
using Flex.Scripts.Core.Inspect.Editor.UIElements.ImGuiDrawer;
using Flex.Scripts.Core.Inspect.Runtime.Interfaces;
using Flex.Scripts.FlexUI.UIShapesKit.Assets.ThisOtherThing.UI_Shapes_Kit.Geometry;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

namespace Flex.Scripts.FlexUI.Theming.Setters
{
    public class RoundingPropertiesSetter : StyleSetter, ICustomDrawer
    {
        public GeoUtils.RoundingProperties RoundingProperties { get; set; } = new GeoUtils.RoundingProperties();
        public override void SetStyle(object target)
        {
            
        }

        public VisualElement DrawProperty()
        {
            IMGUIContainer container = new ImGuiDrawer(Activator.CreateInstance(typeof(SerializedProperty)) as SerializedProperty, typeof(GeoUtils.RoundingProperties).GetIMGUIPropertyDrawerOfType());
            return container;
        }
    }
}