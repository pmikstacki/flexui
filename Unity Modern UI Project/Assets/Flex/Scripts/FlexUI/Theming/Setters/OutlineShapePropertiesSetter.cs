﻿using Flex.Scripts.FlexUI.UIShapesKit.Assets.ThisOtherThing.UI_Shapes_Kit.Geometry;
using Flex.Scripts.FlexUI.UIShapesKit.Assets.ThisOtherThing.UI_Shapes_Kit.Geometry.Shapes;
using UnityEngine;
using UnityEngine.UIElements;

namespace Flex.Scripts.FlexUI.Theming.Setters
{
    public class OutlineShapePropertiesSetter : StyleSetter
    {
        [SerializeField] private GeoUtils.OutlineShapeProperties outlineShapeProperties;

        public GeoUtils.OutlineShapeProperties OutlineShapeProperties { get; set; } =
            new GeoUtils.OutlineShapeProperties();
        
        

        public override void SetStyle(object target)
        {
            if (target is Ellipse ellipse) ellipse.ShapeProperties = outlineShapeProperties;
            if (target is Rectangle rectangle) rectangle.ShapeProperties = outlineShapeProperties;
        }
    }
}