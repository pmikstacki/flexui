﻿using Flex.Scripts.FlexUI.UIShapesKit.Assets.ThisOtherThing.UI_Shapes_Kit.Geometry;
using Flex.Scripts.FlexUI.UIShapesKit.Assets.ThisOtherThing.UI_Shapes_Kit.Geometry.Shapes;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace Flex.Scripts.FlexUI.Theming.Setters
{
    public class ShapePropertiesSetter : StyleSetter
    {
        public GeoUtils.ShapeProperties ShapeProperties
        {
            get;
            set;
        } = new GeoUtils.ShapeProperties();

        
        public override void SetStyle(object target)
        {
            if (target is Polygon polygon) polygon.ShapeProperties = ShapeProperties;
            if (target is Line line) line.ShapeProperties = ShapeProperties;
            if (target is Sector sector) sector.ShapeProperties = ShapeProperties;
        }
    }
}