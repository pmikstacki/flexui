﻿using System;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;
using Button = UnityEngine.UI.Button;
using Slider = UnityEngine.UIElements.Slider;

namespace Flex.Scripts.FlexUI.Theming.Setters
{
    [Serializable]
    public class ButtonStyleSetter : StyleSetter
    {
        public ColorBlock ButtonStyle { get; set; }


        public override void SetStyle(object target)
        {
            if (target is Button button)
            {
                button.colors = this.ButtonStyle;
            }
            else
            {
                Debug.Log("Incompatible Target. Cannot set Button Style on "+target.GetType().ToString());
            }
        }
    }
}