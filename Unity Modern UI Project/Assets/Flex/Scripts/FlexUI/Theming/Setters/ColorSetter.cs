﻿using System;
using TMPro;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;
using Image = UnityEngine.UI.Image;

namespace Flex.Scripts.FlexUI.Theming.Setters
{
    [Serializable]
    public class ColorSetter : StyleSetter
    {

        public Color Color { get; set; }

        
        
        public override void SetStyle(object target)
        {
            if (target is Text text) text.color = Color;
            if (target is TMP_Text tmpText) tmpText.color = Color;
            if (target is Image image) image.color = Color;
            if (target is RawImage rawImage) rawImage.color = Color;
            if (target is MaskableGraphic maskableGraphic) maskableGraphic.color = Color;
        }
        
    }
}