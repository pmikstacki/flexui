﻿using Flex.Scripts.FlexUI.Animation.Utility;
using Flex.Scripts.FlexUI.UIShapesKit.Assets.ThisOtherThing.UI_Shapes_Kit.Geometry;
using Flex.Scripts.FlexUI.UIShapesKit.Assets.ThisOtherThing.UI_Shapes_Kit.Geometry.Shapes;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace Flex.Scripts.FlexUI.Theming.Setters
{
    public class AntiAliasingPropertiesSetter : StyleSetter
    {

        public GeoUtils.AntiAliasingProperties AntiAliasingProperties { get; set; } =
            new GeoUtils.AntiAliasingProperties();
        

        
        
        public override void SetStyle(object target)
        {
            if (target is Sector sector)
            {
                sector.AntiAliasingProperties = AntiAliasingProperties;
                sector.AntiAliasingProperties.UpdateAdjusted(sector.GetComponent<RectTransform>().GetOwnerCanvasComponent());
            }

            if (target is Rectangle rectangle)
            {
                rectangle.AntiAliasingProperties = AntiAliasingProperties;
                rectangle.AntiAliasingProperties.UpdateAdjusted(rectangle.GetComponent<RectTransform>().GetOwnerCanvasComponent());

            }

            if (target is Polygon polygon)
            {
                polygon.AntiAliasingProperties = AntiAliasingProperties;
                polygon.AntiAliasingProperties.UpdateAdjusted(polygon.GetComponent<RectTransform>().GetOwnerCanvasComponent());

            }

            if (target is Line line)
            {
                line.AntiAliasingProperties = AntiAliasingProperties;
                line.AntiAliasingProperties.UpdateAdjusted(line.GetComponent<RectTransform>().GetOwnerCanvasComponent());
            }
            
        }
    }
}