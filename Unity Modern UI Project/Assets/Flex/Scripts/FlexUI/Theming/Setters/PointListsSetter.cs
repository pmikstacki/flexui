﻿using Flex.Scripts.FlexUI.UIShapesKit.Assets.ThisOtherThing.UI_Shapes_Kit.Geometry.Shapes;
using Flex.Scripts.FlexUI.UIShapesKit.Assets.ThisOtherThing.UI_Shapes_Kit.Geometry.ShapeUtils;

namespace Flex.Scripts.FlexUI.Theming.Setters
{
    public class PointListsSetter : StyleSetter
    {
        public PointsList.PointListsProperties PointListsProperties { get; set; } =
            new PointsList.PointListsProperties();
        
        
        public override void SetStyle(object target)
        {
            if (target is Polygon polygon)
            {
                polygon.PointListsProperties = PointListsProperties;
            }
            
            
        }
    }
}