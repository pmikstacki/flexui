﻿using Flex.Scripts.FlexUI.UIShapesKit.Assets.ThisOtherThing.UI_Shapes_Kit.Geometry;
using Flex.Scripts.FlexUI.UIShapesKit.Assets.ThisOtherThing.UI_Shapes_Kit.Geometry.Shapes;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace Flex.Scripts.FlexUI.Theming.Setters
{
    public class OutlinePropertiesSetter : StyleSetter
    {
        [SerializeField] private GeoUtils.OutlineProperties outlineProperties;

        public GeoUtils.OutlineProperties OutlineProperties { get; set; } = new GeoUtils.OutlineProperties();

        public override void SetStyle(object target)
        {
            if (target is Arc arc) arc.OutlineProperties = outlineProperties;
            if (target is Ellipse ellipse) ellipse.OutlineProperties = outlineProperties;
        }
    }
}