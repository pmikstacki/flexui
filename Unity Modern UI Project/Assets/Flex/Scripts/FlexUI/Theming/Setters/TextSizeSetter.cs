﻿using System;
using TMPro;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;

namespace Flex.Scripts.FlexUI.Theming.Setters
{
    [Serializable]
    public class TextSizeSetter : StyleSetter
    {
        public int TextSize
        {
            get;
            set;
        }
        
        public override void SetStyle(object target)
        {
            if (target is TMP_Text tmpText) tmpText.fontSize = TextSize;
                if(target is Text text) text.fontSize = TextSize;
        }
    }
}