using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Flex.Scripts.FlexUI.Theming
{
    [ExecuteInEditMode]
    public class ThemeService
    {

        private Theme currentTheme;
        public Theme CurrentTheme
        {
            set
            {
                currentTheme = value;
                ApplyTheme(value);
            }

            get => currentTheme;
        }

        public List<string> GetThemeElements()
        {
            return currentTheme.ThemeElements.Select(x => x.Name).ToList();
        }
    
        private void ApplyTheme(Theme theme)
        {
        
        }
    
    }
}
