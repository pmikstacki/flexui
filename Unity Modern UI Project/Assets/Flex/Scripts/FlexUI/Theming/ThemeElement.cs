﻿using System;
using System.Collections.Generic;
using Flex.Scripts.Core.Serialize;
using Flex.Scripts.FlexUI.Theming.Setters;
using UnityEngine;

namespace Flex.Scripts.FlexUI.Theming
{
    [Serializable]
    public class ThemeElement : INameable
    {
        public string Name { get; set; }
        [SerializeField] private List<StyleSetter> styleSetters = new List<StyleSetter>();

        public List<StyleSetter> StyleSetters
        {
            get => styleSetters;
            set => styleSetters = value;
        }


       
    }
}