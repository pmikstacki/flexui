﻿using System;
using System.Collections.Generic;
using Flex.Scripts.Core.Serialize;
using UnityEngine;

namespace Flex.Scripts.FlexUI.Theming
{
    [Serializable]
    public class Theme : INameable
    {
        [SerializeField] private List<ThemeElement> themeElements = new List<ThemeElement>();
 
        public List<ThemeElement> ThemeElements
        {
            get { return this.themeElements; }
            set => themeElements = value;
        }

        public string Name { get; set; }
    }
    
}