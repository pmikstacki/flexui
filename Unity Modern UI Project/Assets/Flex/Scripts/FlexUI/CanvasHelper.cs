﻿using UnityEngine;

namespace Flex.Scripts.FlexUI.Animation.Utility
{
    public static class CanvasHelper
    {
        public static RectTransform GetOwnerCanvas(this RectTransform rectTransform)
        {
            var parent = rectTransform.parent.GetComponent<Canvas>();
            if (!parent)
                return parent.GetComponent<RectTransform>().GetOwnerCanvas();
            else
                return parent.GetComponent<RectTransform>();
        }
        public static Canvas GetOwnerCanvasComponent(this RectTransform rectTransform)
        {
            var parent = rectTransform.parent.GetComponent<Canvas>();
            if (!parent)
                return parent.GetComponent<RectTransform>().GetOwnerCanvasComponent();
            else
                return parent.GetComponent<Canvas>();
        }
    }
}