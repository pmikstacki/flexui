﻿using UnityEngine;
using UnityEngine.UI;

namespace Flex.Scripts.FlexUI.UIShapesKit.Assets.ThisOtherThing.UI_Shapes_Kit.Geometry.Shapes
{

	[AddComponentMenu("UI/Shapes/Empty Fill Rect", 200)]
	public class EmptyFillRect : Graphic
	{
		public override void SetMaterialDirty() { return; }
		public override void SetVerticesDirty() { return; }

		protected override void OnPopulateMesh(VertexHelper vh)
		{
			vh.Clear();
		}
	}
}
