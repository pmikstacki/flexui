﻿using Flex.Scripts.Core.Inspect.Editor.Extensions;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;
using EllipseProperties = Flex.Scripts.FlexUI.UIShapesKit.Assets.ThisOtherThing.UI_Shapes_Kit.Geometry.ShapeUtils.Ellipses.EllipseProperties;

namespace Flex.Scripts.FlexUI.UIShapesKit.Assets.ThisOtherThing.UI_Shapes_Kit.Geometry.Editor.CustomDrawers
{
	[CustomPropertyDrawer(typeof(EllipseProperties))]
	public class EllipsePropertiesDrawer : PropertyDrawer
	{
		public override VisualElement CreatePropertyGUI(SerializedProperty property)
		{
			EllipseProperties roundedProperties = 
				(EllipseProperties)fieldInfo.GetValue(property.serializedObject.targetObject);
			var root = new VisualElement();
			if (roundedProperties != null)
			{
				root.Add(new PropertyField(property.FindPropertyRelative("Fitting")));
				root.Add(new PropertyField(property.FindPropertyRelative("BaseAngle")));
				root.Add(new Label("Resolution"));
				var res = new PropertyField(property.FindPropertyRelative("Resolution"));
				var resMaxDistance = new PropertyField(property.FindPropertyRelative("ResolutionMaxDistance"));
				var resFixed = new PropertyField(property.FindPropertyRelative("FixedResolution"));
				resFixed.style.display = DisplayStyle.None;
				res.RegisterValueChangeCallback(e =>
				{
					switch (e.changedProperty.enumValueIndex)
					{
						case 0:
							resFixed.style.display = DisplayStyle.None;
							resMaxDistance.style.display = DisplayStyle.Flex;
							break;
						case 1:
							resMaxDistance.style.display = DisplayStyle.None;
							resFixed.style.display = DisplayStyle.Flex;
							break;
					}
				});
				
				root.Add(res);
				root.Add(resMaxDistance);
				root.Add(resFixed);
			}
			return root;
		}

		public override void OnGUI (Rect position, SerializedProperty property, GUIContent label)
		{
			position.height = EditorGUIUtility.singleLineHeight;
			property.isExpanded = EditorGUI.Foldout(position, property.isExpanded, label);

			if (!property.isExpanded)
				return;

			EditorGUI.BeginProperty(position, label, property);

			EllipseProperties roundedProperties = 
				(EllipseProperties)fieldInfo.GetValue(property.serializedObject.targetObject);

			var indent = EditorGUI.indentLevel;
			EditorGUI.indentLevel = 1;

			Rect propertyPosition = new Rect (position.x, position.y + EditorGUIUtility.singleLineHeight, position.width, EditorGUIUtility.singleLineHeight);

			EditorGUI.PropertyField(propertyPosition, property.FindPropertyRelative("Fitting"), new GUIContent("Fitting"));
			propertyPosition.y += EditorGUIUtility.singleLineHeight;

			EditorGUI.PropertyField(propertyPosition, property.FindPropertyRelative("BaseAngle"), new GUIContent("Base Angle"));
			propertyPosition.y += EditorGUIUtility.singleLineHeight;

			propertyPosition.y += EditorGUIUtility.singleLineHeight;

			EditorGUI.LabelField(propertyPosition, "Resolution");
			propertyPosition.y += EditorGUIUtility.singleLineHeight * 1.25f;

			EditorGUI.PropertyField(propertyPosition, property.FindPropertyRelative("Resolution"), new GUIContent("Mode"));
			propertyPosition.y += EditorGUIUtility.singleLineHeight;

			switch (roundedProperties.Resolution)
			{
				case EllipseProperties.ResolutionType.Calculated:
					EditorGUI.PropertyField(propertyPosition, property.FindPropertyRelative("ResolutionMaxDistance"), new GUIContent("Max Distance"));
					break;
				case EllipseProperties.ResolutionType.Fixed:
					EditorGUI.PropertyField(propertyPosition, property.FindPropertyRelative("FixedResolution"), new GUIContent("Resolution"));
					break;
			}

			EditorGUI.indentLevel = indent;
			EditorGUI.EndProperty();
		}

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			if (!property.isExpanded)
			{
				return EditorGUIUtility.singleLineHeight;
			}

			return EditorGUIUtility.singleLineHeight * 7.25f;
		}
	}
}
