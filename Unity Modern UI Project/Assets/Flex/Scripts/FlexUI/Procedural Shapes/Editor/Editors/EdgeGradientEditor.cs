﻿using UnityEditor;
using UnityEditor.UI;
using UnityEngine;
using EdgeGradient = Flex.Scripts.FlexUI.UIShapesKit.Assets.ThisOtherThing.UI_Shapes_Kit.Geometry.Shapes.EdgeGradient;

namespace Flex.Scripts.FlexUI.UIShapesKit.Assets.ThisOtherThing.UI_Shapes_Kit.Geometry.Editor.Editors
{
	[CustomEditor(typeof(EdgeGradient))]
	[CanEditMultipleObjects]
	public class EdgeGradientEditor : GraphicEditor
	{
		protected SerializedProperty materialProp;
		protected SerializedProperty raycastTargetProp;

		protected SerializedProperty propertiesProp;

		protected override void OnEnable()
		{
			materialProp = serializedObject.FindProperty("m_Material");
			raycastTargetProp = serializedObject.FindProperty("m_RaycastTarget");

			propertiesProp = serializedObject.FindProperty("Properties");
		}

		protected override void OnDisable()
		{
			Tools.hidden = false;
		}

		public override void OnInspectorGUI()
		{
			serializedObject.Update();

			EditorGUILayout.PropertyField(materialProp);
			EditorGUILayout.PropertyField(raycastTargetProp);
			EditorGUILayout.Space();

			EditorGUILayout.PropertyField(propertiesProp, new GUIContent("Edges"), true);

			serializedObject.ApplyModifiedProperties();
		}
	}
}
