﻿using UnityEditor;
using UnityEditor.UI;
using EmptyFillRect = Flex.Scripts.FlexUI.UIShapesKit.Assets.ThisOtherThing.UI_Shapes_Kit.Geometry.Shapes.EmptyFillRect;

namespace Flex.Scripts.FlexUI.UIShapesKit.Assets.ThisOtherThing.UI_Shapes_Kit.Geometry.Editor.Editors
{
	[CustomEditor(typeof(EmptyFillRect))]
	[CanEditMultipleObjects]
	public class EmptyFillRectEditor : GraphicEditor
	{
		protected override void OnEnable()
		{
			base.OnEnable();
		}

		public override void OnInspectorGUI()
		{
			serializedObject.Update();

			RaycastControlsGUI();

			serializedObject.ApplyModifiedProperties();
		}
	}
}
