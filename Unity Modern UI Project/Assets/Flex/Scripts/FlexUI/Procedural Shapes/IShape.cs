﻿namespace Flex.Scripts.FlexUI.UIShapesKit.Assets.ThisOtherThing.UI_Shapes_Kit.Geometry
{
	public interface IShape
	{
		void ForceMeshUpdate();
	}
}
