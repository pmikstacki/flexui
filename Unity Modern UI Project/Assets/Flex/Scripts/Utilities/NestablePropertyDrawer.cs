﻿using UnityEditor;
using UnityEngine;

namespace Flex.Scripts.Utilities
{
    using Object = System.Object;

    public class NestablePropertyDrawer : PropertyDrawer
    {
        protected Object propertyObject = null;

        protected virtual void Initialize(SerializedProperty prop)
        {
            if (propertyObject == null)
            {
                string[] path = prop.propertyPath.Split('.');
                propertyObject = prop.serializedObject.targetObject;
                foreach (string pathNode in path)
                {
                    propertyObject = propertyObject.GetType().GetField(pathNode).GetValue(propertyObject);
                }
            }
        }

        public override void OnGUI(Rect position, SerializedProperty prop, GUIContent label)
        {
            Initialize(prop);
        }
    }
}